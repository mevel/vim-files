" https://vi.stackexchange.com/a/31030

" Enable spellchecking in BibTeX files ONLY in selected fields
" (the following code implicitly disables spellchecking everywhere else).

syntax region bibSpellBrace
  \ start=/\(\(title\|type\|note\)\s*=\s*\)\@<={/
  \ end=/\ze}/ skip=/\(\\[{}]\)/
  \ contained containedin=bibBrace
  \ contains=@Spell
syntax region bibSpellQuote
  \ start=/\(\(title\|type\|note\)\s*=\s*\)\@<="/
  \ end=/\ze"/ skip=/\(\\"\)/
  \ contained containedin=bibQuote
  \ contains=@Spell
