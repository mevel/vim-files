" Match my custom commands \sref, \fref, \fnref for referencing sections,
" figures, footnotes, etc. This prevents the label from getting spellchecked.
syn region texRefZone matchgroup=texStatement
  \ start="\\\([sf]\|fn\)ref{" end="}\|%stopzone\>" contains=@texRefGroup
