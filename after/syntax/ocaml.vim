" Highlight let‐binders, ie. identifiers which are given an immediate value.
" We avoid catching the “exception” and “module” keywords (as in “let exception
" E …” and “let module M …”).
"
" Limitations:
"   — does not recognize let-patterns as in “let (a,b) = …” or “let (C x) = …”
"   — does not recognize comments as in “let (*lol*) f = …”
"   — recognize attributes as in “let[@inline] f = …” only in limited cases
"   — does not support object values and methods
"
" Highlighting other binders (function arguments, “fun”, “function”, “try with”,
" match with”…) is much more difficult and would require writing a more accurate
" parser.
syn match ocamlLetBinder /\(\<\(let\(\_s\+rec\)\?\|and\|for\)\>\_s*\(\[@\(@\|\w\|\.\|\_s\)*\]\_s*\)*\)\@<=\<\(exception\>\|module\>\|open\>\)\@!\(\l\|_\)\(\w\|'\)*\>/

hi link ocamlLetBinder Identifier

" The default syntax file links ocamlLabel to Identifier, which is too heavy.
" Here we use Spring Green with normal font.
hi ocamlLabel ctermfg=48 guifg=#00ff87

" The default syntax file links ocamlQuotedStringDelim to Identifier, which is
" bad from a semantic point of view, even though it looks nice with my theme.
hi link ocamlQuotedStringDelim Comment

" Set a different highlighting for PPX nodes.
hi link ocamlPpxEncl       PreProc
hi link ocamlPpxIdentifier PreProc

" Set a different highlighting for shebangs.
hi link ocamlShebang       Statement

" Highlight module names better (bold white).
hi link ocamlModPath ocamlModule
hi ocamlModule cterm=bold ctermfg=231 gui=bold guifg=#ffffff

" By default, “struct end” and “sig end” are highlighted like ocamlModule, which
" is probably undesirable. Here, we give them the same color as keywords, but we
" put them in bold.
hi link ocamlSigEncl ocamlStructEncl
hi ocamlStructEncl cterm=bold ctermfg=184 gui=bold guifg=#cdd129

" Highlight _ as an identifier (instead of a keyword).
hi link ocamlAnyVar Identifier

" ;; in source files tends to be frowned upon, so we draw attention on it.
" (OLD: matching ";;" is already implemented by the default syntax file, but ";"
" is defined after ";;" in that file, so that it takes precedence over ";;".)
"syn match ocamlTopStop ";;"
hi ocamlTopStop cterm=bold ctermfg=184 ctermbg=234 gui=bold guifg=#d7d700 guibg=#1c1c1c

" Highlight operators distinctly but not in a gaudy color.
"hi ocamlOperator ctermfg=201 guifg=#afaf5f

" Highlight types.
" 112 = light green (the color for the “Type“ highlighting group in my theme)
hi ocamlTypeConstr       ctermfg=112
hi ocamlTypeBuiltin      ctermfg=112 cterm=bold
hi ocamlTypeVar          ctermfg=112 cterm=italic
hi ocamlTypeAnyVar       ctermfg=112 cterm=bold
"hi ocamlTypeVariance     ctermfg=34  cterm=bold
"hi ocamlTypeKeyChar      ctermfg=208 cterm=bold

" Highlight decorative ruler comments.
syn match ocamlRuler "(\*\*\+)"
syn match ocamlRuler "(\*\*\+)" containedin=ocamlDocumentation
syn cluster ocamlCommentLike add=ocamlRuler
hi link ocamlRuler ocamlComment

" doc-comment customizations.
" https://github.com/ocaml/vim-ocaml/pull/106
hi link ocamlDocEncl PreProc
" ^ defined by PR review
hi link odocMarker PreProc
hi link odocCrossrefMarker odocMarker
" ^ defined by PR review
hi link odocTag PreProc
hi link odocCrossrefKw Structure
" ^ or PreProc
hi link odocLinkText odocItalic
hi link odocCode Constant
hi link odocCodeBlock odocCode
hi link odocVerbatim odocCodeBlock

" Experiments for setting text in bold while inheriting the surrounding color:
" https://github.com/ocaml/vim-ocaml/pull/106#discussion_r1564033009
"hi def link ocamlDocumentation Comment
"exec 'hi odocBold term=bold cterm=bold gui=bold'
"  \. ' ctermfg=' . synIDattr(synIDtrans(hlID('ocamlDocumentation')), 'fg', 'cterm')
"  \. ' ctermbg=' . synIDattr(synIDtrans(hlID('ocamlDocumentation')), 'bg', 'cterm')
"  \. ' guifg='   . synIDattr(synIDtrans(hlID('ocamlDocumentation')), 'fg', 'gui')
"  \. ' guibg='   . synIDattr(synIDtrans(hlID('ocamlDocumentation')), 'bg', 'gui')
