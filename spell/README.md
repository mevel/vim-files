This directory contains the French dictionary from upstream Vim, downloaded from 
http://ftp.vim.org/vim/runtime/spell/ , as well as a custom complement for 
general-purpose French: `fr.utf-8.add` .

It also allows a personal dictionary, `custom.utf-8.add`, which is ignored by 
Git since it may contain sensible data.
