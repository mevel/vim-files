"fun! s:DetectMyFileType()
"    if getline(1) =~# 'some regex'
"        setf myfiletype
"    endif
"endfun
"
"au BufNewFile,BufRead *.mft,*.ft setf myfiletype
"au BufNewFile,BufRead * call s:DetectMyFileType()
