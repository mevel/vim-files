" Maintainer:   Glen Mével
" URL:          https://gitlab.crans.org/mevel/vim-files
" Version:      1.4
" Last Change:
"               2024-06-14 - CurSearch, to better distinguish the current match (Glen)
"               2021-03-14 - CursorLineNr, Pmenu*, WildMenu, ErrorMsg, WarningMsg, Spell* (Glen)
"               2018-11-08 - Error and Todo are now more readable (Glen)
"               2018-09-05 - String is now bright blue, instead of orange like Identifier (Glen)
"               2018-09-05 - PreProc is now more distinct; Number is now blue/green instead of light yellow like PreProc (Glen)
"               2018-09-05 - Type and Function are now green, instead of yellow like Keyword (Glen)
"               2015-09-03 - personal alterations (Glen)
"
" adapted from the original Vitamins theme, whose metadata were:
"
" Maintainer:   Henrique C. Alves (hcarvalhoalves@gmail.com)
" Version:      1.1
" Last Change:  September 23 2008

" Appearance in the GUI is not guaranteed. Only color terminals are supported.

set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

" theme name is automatically retrieved from filename:
let g:colors_name = expand('<sfile>:t:r')

" characters used for ASCII drawing
"set fillchars=stl:\ ,stlnc:\ ,vert:│,fold:⋅

" Style for columns highlighted with variable “colorcolumn” (often used to mark
" the 80-columns limit)
if version >= 703
  hi ColorColumn              guibg=#202020 gui=bold                 ctermbg=234  cterm=bold
endif

" Vim >= 7.0 specific colors
if version >= 700
  "hi CursorColumn             guibg=#2d2d2d                          ctermbg=236
  "hi CursorLine               guibg=#2d2d2d                          ctermbg=236  cterm=NONE
  " Highlighting the current line clashes with other highlightings
  " (e.g. http://stackoverflow.com/questions/15980451), so we disable it:
  hi CursorColumn             guibg=#2d2d2d
  hi CursorLine               guibg=#2d2d2d                                       cterm=NONE
  hi CursorLineNr                                       ctermfg=101               cterm=bold
  hi MatchParen guifg=#f6f3e8 guibg=#857b6f gui=bold                 ctermbg=59   cterm=bold
  " we link these styles so that the completion menu has the same style as the
  " “wildmenu” (ie the completion menu for command-line):
  hi! link Pmenu    StatusLine
  hi! link PmenuSel WildMenu
  hi! link PmenuSbar Pmenu
  hi PmenuThumb                                                      ctermbg=237
endif

" General colors
hi Cursor       guifg=NONE    guibg=#165559 gui=NONE    ctermfg=23   ctermbg=241  cterm=NONE
hi Normal       guifg=#f6f3f0 guibg=#242424 gui=NONE    ctermfg=254  ctermbg=235  cterm=NONE
hi NonText      guifg=#808080 guibg=#303030 gui=NONE    ctermfg=242  ctermbg=232  cterm=NONE
hi TabLineFill                                          ctermfg=NONE ctermbg=232  cterm=NONE
hi TabLine                                              ctermfg=240  ctermbg=234  cterm=NONE
hi TabLineSel                                           ctermfg=240  ctermbg=NONE cterm=bold
hi LineNr       guifg=#5c5a4f guibg=#000000 gui=NONE    ctermfg=239  ctermbg=232  cterm=NONE
hi FoldColumn   guifg=#5c5a4f guibg=#000000 gui=NONE    ctermfg=123  ctermbg=232  cterm=NONE
hi SignColumn   guifg=#5c5a4f guibg=#000000 gui=NONE    ctermfg=123  ctermbg=232  cterm=NONE
hi StatusLine   guifg=#f6f3e8 guibg=#444444 gui=NONE    ctermfg=239  ctermbg=232  cterm=NONE
hi StatusLineNC guifg=#857b6f guibg=#444444 gui=NONE    ctermfg=239  ctermbg=232  cterm=NONE
hi WildMenu     guifg=Yellow  guibg=Black   gui=bold    ctermfg=227  ctermbg=232  cterm=bold
hi VertSplit    guifg=#444444 guibg=#444444 gui=NONE    ctermfg=235  ctermbg=232  cterm=NONE
hi Folded       guifg=#a0a8b0 guibg=#384048 gui=NONE    ctermfg=123  ctermbg=24   cterm=NONE
hi Search       guifg=NONE    guibg=NONE    gui=reverse ctermfg=NONE ctermbg=NONE cterm=reverse
hi CurSearch    guifg=NONE    guibg=NONE    gui=reverse,bold,italic ctermfg=NONE ctermbg=NONE cterm=reverse,bold,italic
hi Visual       guifg=NONE    guibg=#2d2d2d gui=NONE    ctermfg=NONE ctermbg=236  cterm=NONE
hi Title        guifg=#f6f3e8 guibg=NONE    gui=bold    ctermfg=208  ctermbg=NONE cterm=bold
hi ErrorMsg     guifg=White   guibg=Red     gui=bold    ctermfg=255  ctermbg=196  cterm=bold
hi WarningMsg   guifg=Black   guibg=Orange  gui=bold    ctermfg=232  ctermbg=214  cterm=bold
hi MoreMsg                                              ctermfg=110
hi Question                                             ctermfg=110
hi SpecialKey   guifg=#808080 guibg=#343434 gui=NONE

" Syntax highlighting
hi Error        guifg=NONE    guibg=#5f0000 gui=NONE    ctermfg=NONE ctermbg=52   cterm=NONE
hi Todo         guifg=#87875f guibg=#1c1c1c gui=bold    ctermfg=101  ctermbg=234  cterm=bold
hi Comment      guifg=#808080 guibg=NONE    gui=italic  ctermfg=244  ctermbg=NONE cterm=italic
hi Constant     guifg=#acf0f2 guibg=NONE    gui=NONE    ctermfg=159  ctermbg=NONE cterm=NONE
hi String       guifg=#00afaf guibg=NONE    gui=italic  ctermfg=37   ctermbg=NONE cterm=italic
hi Identifier   guifg=#ff5d28 guibg=NONE    gui=bold    ctermfg=202  ctermbg=NONE cterm=bold
hi Function     guifg=#87d700 guibg=NONE    gui=NONE    ctermfg=112  ctermbg=NONE cterm=NONE
hi Type         guifg=#87d700 guibg=NONE    gui=NONE    ctermfg=112  ctermbg=NONE cterm=NONE
hi Statement    guifg=#af5f5f guibg=NONE    gui=NONE    ctermfg=131  ctermbg=NONE cterm=NONE
hi Keyword      guifg=#cdd129 guibg=NONE    gui=NONE    ctermfg=184  ctermbg=NONE cterm=NONE
hi PreProc      guifg=#ffd787 guibg=NONE    gui=NONE    ctermfg=222  ctermbg=NONE cterm=NONE
hi Number       guifg=#5fd7af guibg=NONE    gui=NONE    ctermfg=79   ctermbg=NONE cterm=NONE
hi Special      guifg=#acf0f2 guibg=NONE    gui=NONE    ctermfg=159  ctermbg=NONE cterm=NONE

" Spell checking
hi SpellBad   guifg=NONE guibg=NONE guisp=Red       gui=undercurl  ctermfg=NONE ctermbg=52 cterm=underline
hi SpellCap   guifg=NONE guibg=NONE guisp=Magenta   gui=undercurl  ctermfg=NONE ctermbg=55 cterm=underline
hi SpellRare  guifg=NONE guibg=NONE guisp=SlateBlue gui=undercurl  ctermfg=NONE ctermbg=19 cterm=underline
hi SpellLocal guifg=NONE guibg=NONE guisp=SlateBlue gui=undercurl  ctermfg=NONE ctermbg=19 cterm=underline
