#!/usr/bin/sh

UNDODIR=undo

files=( "$UNDODIR"/%* )

files_ok=( )
files_ko=( )

for f in "${files[@]}" ; do
	g="${f#$UNDODIR/}"
	g="${g//%//}"
	if [ -e "$g" ] ; then
		files_ok+=( "$f" )
	else
		files_ko+=( "$f" )
	fi
done

printf -- 'ok = %u\nko = %u\n' "${#files_ok[@]}" "${#files_ko[@]}"

for f in "${files_ko[@]}" ; do
	g="${f#$UNDODIR/}"
	g="${g//%//}"
	printf '  %s\n' "$g"
done

[ ${#files_ko[@]} -eq 0 ] && exit

mkdir -pv "undo_ko/"
exec mv -t "undo_ko/" -- "${files_ko[@]}"
