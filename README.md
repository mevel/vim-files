# Glen’s Vim files

This is my personal Vim setup. It includes an extensive, longly crafted `vimrc`, 
a custom colorscheme, and my set of plugins. In addition to general-purpose 
stuff, there are settings for:

- the French language;
- the BÉPO keyboard layout;
- several programming languages, most notably OCaml and LaTeX.

Plugins are found in directory `bundle/`. Each plugin is a Git submodule. 
Therefore, be sure to use `--recursive` when git-cloning.
