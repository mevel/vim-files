""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Vim / NeoVim local configuration                       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" This file is read by both Vim and NeoVim. Hence it should be saved as 
" ~/vim/vimrc and symlinked from ~/.config/nvim/init.vim (or the converse).

if has('nvim')
  " Make NeoVim use Vim’s global configuration:
  set rtp^=/usr/share/vim/vim82
  set rtp^=/usr/share/vim/vimfiles
  set rtp+=/usr/share/vim/vimfiles/after
  set packpath^=/usr/share/vim/vim82
  " Make NeoVim use Vim’s local configuration:
  "set rtp^=~/.vim
  "set rtp+=~/.vim/after
  "set packpath^=~/.vim
  " As an alternative, we can merge Vim’s and NeoVim’s local configuration by 
  " symlinking ~/.config/nvim to ~/.vim and, within that directory, init.vim to 
  " vimrc (or the converse). In that case, I am not sure whether adding 
  " redundant paths to the runtime paths is safe.
elseif version >= 800
  " Vim 8 ships with better defaults, inspired by NeoVim; load them.
  unlet! skip_defaults_vim
  source $VIMRUNTIME/defaults.vim
endif

" Note:
"   some ideas there: https://github.com/mmottl/vim-files/blob/master/.vimrc

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General settings                                                             "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Do not write a global session file.
set viminfo=
let g:netrw_dirhistmax = 0

" Enable swap files.
set swapfile
" Store swap files in a central directory, instead of alongside the files.
"set directory=~/.cache/vim/swap//
set directory=~/.vim/swap//
if ! has('nvim') && ! isdirectory(expand(&g:directory))
  silent! call mkdir(expand(&g:directory), 'p', 0700)
endif
"set directory+=.
" Don’t prompt what to do when the swap file already exists; just recover it (?).
set shortmess+=A

" Enable the persistent undo history.
set undofile
set undolevels=1000
set undoreload=10000
" Store undo files in a central directory, instead of alongside the files.
"set undodir=~/.cache/vim/undo//
set undodir=~/.vim/undo//
if ! has('nvim') && ! isdirectory(expand(&g:undodir))
  silent! call mkdir(expand(&g:undodir), 'p', 0700)
endif
"set undodir+=.

" Enable syntax highlighting.
" NOTE:
"   Some syntax-related options are erased when syntax is enabled, so we must do 
"   it early in this file.
syntax enable

" Color scheme.
set t_Co=256
set background=dark    " if Vim is running over a dark background
colorscheme Vitamins-Glen

" Font (GUI only).
" For the choice of mono fonts, see my ~/.Xdefaults.
set guifont=DejaVu\ Sans\ Mono\ 13,Liberation\ Mono\ 13
" increase line height in case the font is too tight vertically:
"set linespace=3

" Interface.
set laststatus=2
set shortmess-=f
set shortmess-=F
set shortmess+=r
set shortmess+=I
set showcmd
set noshowmode " the mode is already displayed by Airline’s bar
set ruler
" These settings leave just 80 columns for the text area when Vim is layed out 
" on half of my 13-inch display; otherwise I’d like to increase 'foldcolumn' to 
" at least 3.
set number numberwidth=4
set foldcolumn=2
set fillchars=foldclose:>,foldopen:v,foldsep:│,fold:>
set showtabline=2
set novisualbell
set mouse=a
set scrolloff=3
set virtualedit=block,onemore

" Do not display macro steps.
set lazyredraw

" Highlight cursor line and column.
set cursorline "cursorcolumn
"au WinEnter * set cursorline "cursorcolumn
"au WinLeave * set nocursorline "nocursorcolumn
" Do not move the cursor to the first line when scrolling.
set nostartofline

" Column limit.
set textwidth=80
" different highlight style for the columns past that limit (note that this may 
" override the highlighting for e.g. spellchecking or current line):
let &colorcolumn= '+' . join(range(1,255), ',+')
" same thing but only applies to actual characters past the limit (useful 
" because the technique above is limited to 255 columns):
" Note: This does not adapt to the value of 'textwidth', 80 is hardcoded.
au Syntax * syn match ColorColumn /\%>80v./

" Soft line-breaking.
" Note:
"   <Up> / <Down> / <Home> / <End> (or k/j/^/0/$) navigate physical lines; to 
"   navigate logical lines, the movements can be preceded by g: g<Up>, gj, etc. 
"   It is also possible to remap them so as to have this behavior by default;
"   see https://vim.fandom.com/wiki/Move_cursor_by_display_lines_when_wrapping
"   and https://stackoverflow.com/a/21000307/4615179
set wrap linebreak
" FIXME: as of 2024, Vim still has glitches with multibyte characters here:
set showbreak=»
set breakindent breakindentopt=shift:-1 " minus 1 to compensate for 'showbreak'
set cpoptions+=n

" Display the end of the previous line even if it does not fit on the screen.
set smoothscroll

" Display the beginning of the next line even if it does not fit on the screen.
set display-=truncate
set display+=lastline

" Display special characters as <hex> (instead of e.g. ^X).
set display+=uhex

" Highlight (narrow‐)non‐breaking spaces (U+A0, U+202F) and trailing spaces.
"au Syntax * syn match ExtraWhitespace /\s\+$/
"au Syntax * syn match Nbsp  /\%uA0/
"au Syntax * syn match Nnbsp /\%u202F/
""hi ExtraWhitespace ctermbg=red guibg=red
""hi Nbsp            ctermbg=241 guibg=black
""hi Nnbsp           ctermbg=246 guibg=black
"hi ExtraWhitespace ctermfg=blue guifg=blue  cterm=reverse gui=reverse
"hi Nbsp            ctermfg=241  guifg=black cterm=reverse gui=reverse
"hi Nnbsp           ctermfg=246  guifg=black cterm=reverse gui=reverse
" there is a builtin feature! highlighting works better (not hidden by other 
" highlighting as is the case with the hack above), and it can also customize 
" tabs, EOL and other indicators; however it does not distinguish between NBSP 
" and NNBSP, and trailing tabs are not handled:
set list
if $TERM ==? 'linux'
  " if running in a TTY, use characters that my TTY is able to display:
  set listchars=nbsp:°,trail:¨,tab:›\ ¦,precedes:«,extends:»
else
  " funny chars: ›┆•ˍ˽␣⍽¬
  set listchars=nbsp:°,trail:ˍ,tab:›\ ┆,precedes:«,extends:»
endif

" For more general-purpose highlighting (such as shebangs and email addresses), 
" see below in this vimrc.

" Spell-checking.
" use a file for custom words in the current directory, then another one in the 
" parent directory, then in a user-central place.
" Note:
"   Use count 1 or 2 to access each (e.g. `zg` adds a word to the 
"   directory-local dictionary, `2zg` adds it to the user-central dictionary).
set spellfile=vimspell.utf-8.add,../vimspell.utf-8.add,~/.vim/spell/custom.utf-8.add
" at startup, re-generate custom spell files if needed:
"   https://vi.stackexchange.com/a/5052
for d in glob('./*.add', 1, 1) + glob('../*.add', 1, 1) + glob('~/.vim/spell/*.add', 1, 1)
  if filereadable(d) && (!filereadable(d . '.spl') || getftime(d) > getftime(d . '.spl'))
    exec 'mkspell! ' . fnameescape(d)
  endif
endfor

" A better command completion.
set wildmenu
set wildmode=longest:full,full
set wildignorecase
" this list of ignored file types is kept in sync with  ~/.gitconfig_gitignore
set wildignore+=*.bak,*~,*.sw[po],.fuse_hidden*
set wildignore+=*.bmp,*.gif,*.jpeg,*.jpg,*.png,*.webp
set wildignore+=*.avi,*.mp3,*.mp4,*.mkv,*.webm
set wildignore+=*.pdf,*.ps,*.eps
set wildignore+=*.zip,*.rar,*.7z,*.Z,*.gz,*.tgz,*.bz,*.tbz,*.bz2,*.tbz2,*.xz,*.txz,*.zst
set wildignore+=*.o,*.a,*.exe
set wildignore+=__pycache__,*.pyc
set wildignore+=*.cm[ioxat],*.cmx[as],*.cmti,*.annot,*.byte,*.native
set wildignore+=*.vo,*.vio,*.vo[ks],.*.aux,*.glob,#*.v#,*.crashcoqide,.lia.cache,.nia.cache,.nra.cache
set wildignore+=*.ans,*.aux,*.bbl,*.blg,*.brf,*.cut,*.def,*.dvi,*.fdb_latexmk,*.fls,*.fmt,*.idx,*.ilg,*.ind,*.inx,*.lof,*.lot,*.nav,*.ndx,*.nnd,*.out,*.raux,*.snm,*.synctex.gz,*.thm,*.tns,*.toc,*.vrb,*.waux,*.wdvi,*.xcp

" Searching.
set ignorecase
set smartcase
set gdefault
set incsearch
set hlsearch
set showmatch
set cpoptions-=c
set shortmess-=S  " but redundant with Airline’s bar
set shortmess+=s  " but redundant with Airline’s bar

" Code folding.
set foldmethod=syntax
set foldlevelstart=0  " close all folds upon file opening
set foldnestmax=2     " does not seem to have an effect?

" Code completion.
set completeopt=menuone,preview,longest

" Indenting.
" if set, insert only spaces, otherwise insert \t and complete with spaces:
set expandtab
" length of an actual \t character:
set tabstop=2
" length to use when editing text (eg. TAB and BS keys)
" (0 for ‘tabstop’, -1 for ‘shiftwidth’):
set softtabstop=-1
" length to use when shifting text (eg. <<, >> and == commands)
" (0 for ‘tabstop’):
set shiftwidth=0
" round indentation to multiples of 'shiftwidth' when shifting text
" (so that it behaves like Ctrl-D / Ctrl-T):
set shiftround
" reproduce the indentation of the previous line:
set autoindent
" do not discard the indent produced by 'autoindent' if leaving the line blank:
set cpoptions+=I

" format comments when typing (break lines past the limit, handle delimiters):

" Text formatting.
"
" Note:
"   We don’t always want the following flags, it all depends on the situation:
"     "w", "a", "t".
"   Thus, below in this vimrc, we define keyboard shortcuts to toggle them (note 
"   that we can disable all formatting features at once by toggling paste mode).
"
" if set, use the following convention for wrapping paragraphs: if a line is 
" suffixed with a whitespace, then its paragraph continues onto the next line, 
" otherwise its paragraph ends there (this is a nice convention, and it makes 
" the formatting option "a" less intrusive):
set formatoptions+=w
" if set, auto-wrap the whole current paragraph while typing
" (if 'formatoptions' contains "c" but no "t", then it only applies to comments; 
" even so, this is too intrusive, unless 'formatoptions' also contains "w" since 
" the latter drastically restricts what is considered as the current paragraph):
set formatoptions+=a
" if set, break lines when typing past the limit (applies to any line; we may 
" want it for text-like filetypes, like Markdown, but we probably don’t want it 
" for code-like filetypes):
set formatoptions-=t
" break COMMENT lines when typing past the limit, and take care of inserting the 
" adequate comment delimiter, also when using `gq`
" (if 'formatoptions' contains "t" but not "c", then lines inside comments are 
" broken anyway, but with no comment delimiter, which is bad; hence we want this 
" option to be always set):
set formatoptions+=c
" more sensible options to handle comment delimiters (we always want them on):
set formatoptions+=qjro/
" when joining lines, do not insert two spaces after a dot:
set nojs
" do not break lines which were already too long when insertion began
" (ignored if 'formatoptions' contains "a"):
set formatoptions+=l
" when wrapping text, do not break after a single-letter word:
set formatoptions+=1
" when wrapping text, recognize lists ('autoindent' must be set):
" Note:
"   - Flag "n" is for “numbered” lists (as matched by option 'formatlistpat').
"   - Flag "q" is for “unnumbered” lists (as matched by option 'comments'), 
"     since Vim conflates the notion of “comment” with that of “bulleted list”, 
"     and also with that of “email-style quote block”.
set formatoptions+=qn
" In fact, even though Vim by default uses 'comments' for the bullets - and *,
" we can use 'formatlistpat' for every kind of list. This is better because:
"   - all kinds of lists are then configured in a single place;
"   - option 'comments' is a bit hard to use;
"   - option 'comments' is reset to different values by most file types;
"   - with 'formatlistpat' (by contrast with 'comments'), when pressing `o` in 
"     Normal Mode or <Enter> in Insert Mode while on the 1st line of a list 
"     item, no extra indent is added, which allows to type the next bullet.
" There does not seem to be any caveat to doing that, because, as of Vim 9.1, 
" 'formatlistpat' apparently works inside comments too (despite the fact that 
" its regexes start with '^'), and it works with command `gq`. It doesn’t seem 
" necessary to remove the conflicting patterns from 'comments'
fun! AddListPat(pat)
  " the bullet can be preceeded by any number of blank characters, and it must 
  " be followed by at least one blank:
  let l:pat = '^\s*' . a:pat . '\s\+'
  if &formatlistpat ==# ''
    let &formatlistpat = l:pat
  else
    let &formatlistpat = &formatlistpat . '\|' . l:pat
  endif
endfun
" remove built-in bullets (by default, Vim recognizes "123 " with no punctuation 
" as a bullet, which is bad because numbers in wrapped text often fall at the 
" start of a line, where they provoke accidental indenting of following lines):
set formatlistpat=
" recognize numbered bullets of the form "123. " or "123) " or "(123) ":
call AddListPat('\d\+[).]')
call AddListPat('(\d\+)')
" recognize many kinds of unnumbered bullets (including -, + and *):
call AddListPat('[-+*•—−±~?✓✔→]\+')

" When using Ctrl-A / Ctrl-X, do not treat numbers starting with 0 as octal.
set nrformats-=octal
" Also, ignore leading dashes i.e. treat all numbers as unsigned (for dates).
set nrformats+=unsigned

" When inserting the output of a shell command, do not include stderr.
"set shellredir=>

" Register to use by default for yanks/cuts/pastes.
"set clipboard^=unnamed     " register "* (the X11 selection ‘primary’)
set clipboard^=unnamedplus " register "+ (the X11 selection ‘clipboard’)
" Make the clipboard persist for other applications after Vim has exited
" ( https://stackoverflow.com/questions/6453595 ):
autocmd VimLeave * call system('xsel -ib', getreg('+'))  " or 'xclip -i -sel c'

" Make Bash aliases available from Vim (each command :! will source that file).
" 2024-06: does not seem to work anymore?
let $BASH_ENV = '$HOME/.bash_aliases'

" Allow “vim -p” to open more than 10 tabs.
set tabpagemax=100

" Automatically change the working dir to the dir of the file being edited.
"autocmd BufEnter * lcd %:p:h
set autochdir

" When switching between buffers, if there is a tab/window already displaying
" the target buffer, jump to it, otherwise open a new tab:
set switchbuf=usetab,newtab

" Ask for confirmation instead of failing when exiting while there are unsaved 
" changes, and no ! is specified.
set confirm

" Automatically update files changed outside of Vim.
set autoread

" Set buffer-local options when creating the buffer instead of when entering it.
" TODO: test this option:
" 2024: it has been there for a while and does not seem to cause any harm.
set cpoptions-=s

" Enable filetype-specific indentation and plugins.
filetype plugin indent on
" NOTE: We have already enabled syntax highlighting, we must not do it again or 
" some of our highlighting settings would be discarded; i.e., we must not do:
"     syntax enable

" Hide dotfiles by default in directory listings (use gh to toggle hidden):
"     https://vi.stackexchange.com/a/18678
let g:netrw_hide = 1
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General-purpose highlighting and spell-checking                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Highlight shebangs and modelines in all files, whatever their filetypes.
" This also allows to disable spell-checking in these lines.
au Syntax * syn match Shebang /\%1l^#!.*$/ contains=@NoSpell
hi def link Shebang Statement
" See `:help modeline`; after "vim:" or similar prefix, the 1st modeline form 
" does not start with "se[t] " and it eats everything up to the end of the line:
au Syntax * syn match Modeline /^\%(.\{-}\s\)\?\%(vim\?\|\s\@<=ex\)\%([=<>]\?\d\d\d\)\?:\%( \?set\? \)\@!.*$/ contains=@NoSpell
" the 2nd form starts with "se[t] " and stops at a non-escaped ":" :
au Syntax * syn match Modeline /^\%(.\{-}\s\)\?\%(Vim\|vim\?\|\s\@<=ex\)\%([=<>]\?\d\d\d\)\?: \?set\? .*\\\@<!:.*/ contains=@NoSpell
" Emacs-style modelines (they are called “local variables”):
"     https://emacs.stackexchange.com/questions/36525
"     https://www.gnu.org/software/emacs/manual/html_node/emacs/Specifying-File-Variables.html
" the short form (-*- stuff -*-) is only allowed on the 1st or 2nd line:
au Syntax * syn match Modeline /\%<3l^.\{-}-\*-.*-\*-.*/ contains=@NoSpell
" the long form spans multiple lines (which should appear near end of file):
au Syntax * syn region Modeline start=/^.*Local Variables:/ end=/End:.*$/ contains=@NoSpell
hi def link Modeline Statement

" This function allows to define exceptions to spell-checking.
"     https://vi.stackexchange.com/questions/3990
fun! MatchNoSpell(group, regex)
  let l:cmd = 'au Syntax * syn match ' . a:group . ' /'.a:regex.'/ contains=@NoSpell'
  exe l:cmd . ' containedin=@AllSpell transparent'
  exe l:cmd
endfun
" TODO do `contains=.*Comment,.*String` ?
au Syntax * syn cluster AllSpell contains=
\cComment,cCommentL,cString,cCppString,
\javaComment,javaLineComment,javaCommentTitle,javaString,
\javascriptComment,javascriptLineComment,jsonString,
\scalaMultilineComment,scalaTrailingComment,
\hsLineComment,hsBlockComment,
\ocamlComment,ocamlString,opamComment,opamString,
\lispComment,lispCommentRegion,lispString,
\makeComment,
\shComment,shSingleQuote,shDoubleQuote,zshComment,
\vimComment,vim9Comment,vimLineComment,vim9LineComment,vimMtchComment,vimString,
\pythonComment,pythonString,pythonRawString,
\luaComment,luaString,luaString2,
\perlComment,perlPOD,
\prologComment,prologCComment,prologString,
\texComment,bibField,bibComment,bibComment2,bibComment3,bibQuote,bibBrace,bibParen,
\xmlComment,xmlString,htmlComment,cssComment,
\markdownItalic,markdownBold,markdownBoldItalic,markdownH1,markdownH2,markdownH3,markdownH4,markdownH5,markdownH6,

" Avoid spell-checking URLs and email address.
"     https://stackoverflow.com/questions/1856785/characters-allowed-in-a-url
call MatchNoSpell('Url',
  \ '\w\+:\/\/[:/?#[\]@!$&''()*+,;=0-9[:lower:][:upper:]_\-.~]\+' )
call MatchNoSpell('Email',
  \ '\<\%(mailto:\)\?\.\@![A-Za-z0-9+.-]\+\.\@<!@\%(-\@![A-Za-z0-9-]\+-\@<!\.\)\+[A-Za-z]\{2,}\>')
" We may also highlight them, if we want to (my term already underlines URLs):
"hi def link Url Underlined
hi def link Email Underlined

" Circumvent shortcomings in the French spell dictionary…
"
" (1) ASCII apostrophes are considered by Vim to be part of words, therefore the 
"     dictionary must contain concatenations such as "l'élision" in order to not 
"     have them highlighted as spelling errors. But there are just too many 
"     combinations and the provided French dictionary doesn’t have them all, and 
"     we don’t have them for custom words; so here we catch elided words such as 
"     "l'", "j'", "qu'", etc. if followed by a vowel or "h".
call MatchNoSpell('SpellBreaker',
  \ '\c\<\%([dljmtsn]\|qu\)[''’][[=a=][=e=][=i=][=o=][=u=][=y=]æœh]\@=' )
"
" (2) ASCII hyphens are also part of words. But there are some systematic 
"     combinations that the French dictionary lacks, such as reversed pronouns 
"     as in "dit-il", or things like "ce jour-là". For each verb there would be 
"     hundreds of combinations with one or more pronouns.
"     Here we avoid most false positives by catching things of the form "XXX-il" 
"     but the drawback is that "XXX" is not spell-checked. Hard to do better!
"        https://vi.stackexchange.com/questions/37693/spellcheck-only-a-part-of-a-word
"
" (2a) Certains adverbes se collant après des noms, p.ex. 'ce jour-{CI|LÀ}'.
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]‐-]\+[-‐]\%(ci\|là\)\>' )
"
" (2b) Inversion sujet-verbe.
" 'dis-{JE|TU|NOUS|VOUS}' (1re ou 2e personne)
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[-‐]\%(je\|tu\|[nv]ous\)\>' )
" 'dit-{IL(S)|ELLE(S)|ON}' (3e personne après 't' ou 'd')
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[-‐]\@<![dt][-‐]\%(ils\?\|elles\?\|on\)\>' )
" 'pense-T-{IL|ELLE|ON}' (3e personne avec liaison '-t-')
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[[=a=][=e=][=i=]][-‐]t[-‐]\%(il\|elle\|on\)\>' )
"
" (2c) Impératif.
" 'donne-{MOI|TOI|LE|LA|NOUS|VOUS|LES}' (pronom COD)
" 'donne-{MOI|TOI|LUI|NOUS|VOUS|LEUR}' (pronom COI)
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[-‐]\%([mt]oi\|les\?\|la\|[nv]ous\|lui\|leur\)\>' )
" 'donne-{LE|LA|LES}-{MOI|TOI|LUI|NOUS|VOUS|LEUR}' (pronom COD + pronom COI)
call MatchNoSpell('SpellBreaker',
 \ '\c\<[[:lower:][:upper:]]\+[-‐]\%(les\?\|la\)[-‐]\%([mt]oi\|lui\|[nv]ous\|leur\)\>' )
" 'donnes-EN' ('en' COD/COI)
" 'attends-Y' ('y' COI/CC)
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[sz][-‐]\%(en\|y\)\>' )
" 'informe-[MTL]’EN' (pronom COD singulier + 'en' COI)
" 'attends-[MTL]’Y'  (pronom COD singulier + 'y' COI/CC) [non standard]
" 'va-T’EN'          (verbe pronominal tu + 'en' COI)
" 'rends-T’Y'        (verbe pronominal tu + 'y' COI/CC) [non standard]
" 'donne-[MT]’EN'    ('en' COD + pronom COI moi/toi)
" 'parle-[MT]’Y'     ('y' CC + pronom COI moi/toi) [non standard]
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[-‐][mtl][''’]\%(en\|y\)\>' )
" 'informe-{NOUS|VOUS|LES}-EN'    (pronom COD pluriel + 'en' COI)
" 'attends-{NOUS|VOUS|LES}-Y'     (pronom COD pluriel + 'y' COI/CC)
" 'allons-{NOUS|VOUS}-EN'         (verbe pronominal nous/vous + 'en' COI)
" 'rendons-{NOUS|VOUS}-Y'         (verbe pronominal nous/vous + 'y' COI/CC)
" 'donne-{LUI|NOUS|VOUS|LEUR}-EN' ('en' COD + pronom COI sauf moi/toi)
" 'parle-{LUI|NOUS|VOUS|LEUR}-Y'  ('y' CC + pronom COI sauf moi/toi) [rare]
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[-‐]\%([nv]ous\|les\|lui\|leur\)[-‐]\%(en\|y\)\>' )
" 'donnes-EN-{LUI|LEUR}' ('en' COD + pronom COI 3e personne) [non standard]
" 'parles-Y-{LUI|LEUR}'  ('y' CC + pronom COI 3e personne) [non standard]
call MatchNoSpell('SpellBreaker',
  \ '\c\<[[:lower:][:upper:]]\+[sz][-‐]\%(en\|y\)[-‐]\%(lui\|leur\)\>' )
" NOTES:
"
" La règle pour les combinaisons de deux pronoms à l’impératif est la suivante: 
" les pronoms "le/la/l’/les" sont toujours au début, "en/y" toujours à la fin.
"
"     https://bescherelle.ca/imperatif-et-pronoms/
"     http://orthonet.sdv.fr/pages/informations_p10.html
"         (voir «mots conjoints et mots disjoints»)
"     https://www.languefrancaise.net/forum/viewtopic.php?id=9643
"
" Selon Orthonet, les combinaisons "-m’y" et "-t’y" sont exclues de l’usage 
" (quid de "-l’y"?), mais ici on les autorise, par régularité.
"
" Ici, on se permet une déviation du français standard pour les formes 
" "-lui-{en|y}" et "-leur-{en|y}" ("donne-lui-en, donne-leur-en, parle-lui-y, 
" parle-leur-y") qui sont en théorie correcte mais vraiment hideuses (euphonie), 
" et pour lesquelles il semble parfaitement acceptable d’écrire plutôt 
" "-{en|y}-lui" et "-{en|y}-leur" ("donnes-en-lui, donnes-en-leur, parles-y-lui, 
" parles-y-leur"). Ici on accepte les deux formes.
"
" On pourrait presque accepter "(-en-moi, -en-toi,) -en-nous, -en-vous" aussi. 
" Ce qui permettrait de suivre le principe général suivant: COD puis COI (sauf 
" éventuellement "moi/toi" qui s’abrègerait "m’/t’" et passerait devant "en").

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Custom commands                                                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Do not apply 'langmap' translation to characters emitted by a mapping
" (having this option on by default is a misfeature of Vim).
set nolangremap

" Set a very short timeout for composite key codes sent by the terminal. This 
" works because the terminal is expected to be fast, by contrast with a human. 
" For example, The Escape key (keycode ^[ ) will be interpreted as such without 
" the user waiting, even if there is a custom key binding for the Left Arrow key 
" (keycode ^[[D ) or some other “special” key.
set ttimeout ttimeoutlen=50

" Prefix for filetype‐specific plugins’ shortcuts
" (more suitable for AZERTY or BÉPO layouts than the default ‘\’).
let maplocalleader = ','
let mapleader = ';'

" ‘:H’ is a shortcut for ‘:tab help’
" (we use an abbreviation here because ":H" is a prefix to other commands so 
" ‘:cmap’ wouldn’t work, and also to get command completion; on the other hand, 
" abbreviations don’t work in paste mode).
cnoreabbrev <expr> H getcmdtype() ==# ':' && getcmdline() ==# 'H' ? 'tab help' : 'H'

" ‘:E’ is a shortcut for ‘:tabe’
" (we use an abbreviation, which doesn’t work in paste mode (see ‘:H’ above)).
cnoreabbrev <expr> E getcmdtype() ==# ':' && getcmdline() ==# 'E' ? 'tabe' : 'E'

" A command to toggle wrapping.
nnoremap <silent> <LocalLeader>w :set wrap!<CR>

" A command to toggle listing mode (ie. the graphical displaying of blank chars).
nnoremap <silent> <LocalLeader>l :set list!<CR>

" A command to un‐highlight search results.
nnoremap <silent> <LocalLeader>h :nohlsearch<CR>
" also alias ‘:no’ to that (overrides a builtin command that aliases ‘:map’)
" (we use an abbreviation for the same reason as with ‘:H’ (see above) and also 
" to get completion, but it does not work in paste mode).
cnoreabbrev <expr> no getcmdtype() ==# ':' && getcmdline() ==# 'no' ? 'nohlsearch' : 'no'

" A command to toggle paste mode.
set pastetoggle=<C-P>

" Commands to toggle the flags "w", "a", "t" of 'formatoptions' (see above).
let b:formatoptions_had_a_without_t = v:true
fun! ToggleFormatOptions(flag)
  " toggle the flag given as argument; when setting "t", we also set "a", and 
  " when unsetting "t" we restore "a" to its former state; this is because we 
  " hardly ever want "t" without "a".
  if stridx(&formatoptions, a:flag) >= 0
    execute 'set formatoptions-=' . a:flag
    if a:flag ==# 't' && !b:formatoptions_had_a_without_t
      set formatoptions-=a
      echo 'formatoptions-=ta'
    else
      echo 'formatoptions-=' . a:flag
    endif
  else
    execute 'set formatoptions+=' . a:flag
    if a:flag ==# 't'
      let b:formatoptions_had_a_without_t = (stridx(&formatoptions, 'a') >= 0)
    endif
    if a:flag ==# 't' && !b:formatoptions_had_a_without_t
      set formatoptions+=a
      echo 'formatoptions+=ta'
    else
      echo 'formatoptions+=' . a:flag
    endif
  endif
endfun
nnoremap <silent> <LocalLeader>fw :call ToggleFormatOptions('w')<CR>
vnoremap <silent> <LocalLeader>fw <Esc>:call ToggleFormatOptions('w')<CR>gv
nnoremap <silent> <LocalLeader>fa :call ToggleFormatOptions('a')<CR>
vnoremap <silent> <LocalLeader>fa <Esc>:call ToggleFormatOptions('a')<CR>gv
nnoremap <silent> <LocalLeader>ft :call ToggleFormatOptions('t')<CR>
vnoremap <silent> <LocalLeader>ft <Esc>:call ToggleFormatOptions('t')<CR>gv

" Save modifications with Ctrl-S (Normal Mode, Visual/Select Mode, Insert Mode) 
" (typing Ctrl-S requires XOFF / XON signals to be disabled in the terminal).
nnoremap <silent> <C-S> :update<CR>
vnoremap <silent> <C-S> <Esc>:update<CR>gv
inoremap <silent> <C-S> <C-o>:update<CR>

" Save modifications and “make” with Ctrl-Q (Nor.Mode, Vis/Sel.Mode, Ins.Mode) 
" (ditto regarding XOFF / XON).
nnoremap <silent> <C-Q> :update\|!make<CR>
vnoremap <silent> <C-Q> <Esc>:update\|!make<CR><CR>gv
inoremap <silent> <C-Q> <C-o>:update\|!make<CR>

" Make tilde ~ behave as an operator.
set tildeop

" Make <Del>, ‘x, X’ (Normal Mode, Visual Mode) discard the deleted text, 
" instead of saving it into a register (we can use ‘d’ for cutting a few chars).
" Note:
"   point of friction after a few days: I am used to press ‘xp’ to swap 2 chars; 
"   I can press ‘d<Space>p’ instead. no other point of friction met.
"   (<Del> in Normal Mode is remapped to ‘dl’ because remapping it to <Del> does 
"   not work, apparently the latter gets translated to ‘dl’)
nnoremap <Del> "_dl
xnoremap <Del> "_<Del>
nnoremap x "_x
xnoremap x "_x
nnoremap X "_X
xnoremap X "_X
" Command ‘D’ (Normal Mode, Visual Mode) is used like ‘d’ (‘DD’ or ‘D{motion}’) 
" but discards the deleted text, instead of saving it into a register
" (overrides the builtin command ‘D’ which aliases ‘d$’).
nnoremap DD "_dd
nnoremap D "_d
onoremap D d
xnoremap D "_d
" Make ‘s, S, c’ (‘cc’ or ‘c{motion}’), ‘C, R’ discard the deleted text.
nnoremap s "_s
xnoremap s "_s
nnoremap S "_S
xnoremap S "_S
nnoremap cc "_cc
nnoremap c "_c
xnoremap c "_c
nnoremap C "_C
xnoremap R "_R
" Command ‘C’ (Normal Mode, Visual Mode) is used like ‘c’ (‘cc’ or ‘c{motion}’) 
" but DOES save the deleted text into a register
" (overrides the builtin command ‘C’ which aliases ‘c$‘).
nnoremap CC cc
nnoremap C c
onoremap C c
xnoremap C c

" Command ‘S’ (Normal Mode, Visual Mode) is now the same as ‘s’ but backwards
" (consistency with ‘x’/‘X’; overrides builtin command ‘S’ which aliases ‘cc’).
" (‘S’ in Visual Mode is later overridden by plugin “surround”.)
nnoremap S "_ch

" Make ‘gp, gP’ move the cursor to the end/beginning of the pasted text,
" and ‘p, P’ leave it at the initial position (i.e. before/after pasted text),
"
" In other words:
"   ‘p’ char-wise -> before first pasted char = initial position
"   ‘p’ line-wise -> end of line before first pasted line = end of initial line
"   ‘gp’ char-wise -> last pasted char
"   ‘gp’ line-wise -> end of last pasted line
"   ‘P’ char-wise -> after last pasted char = initial position
"   ‘P’ line-wise -> beg. of line after last pasted line = beg. of initial line
"   ‘gP’ char-wise -> first pasted char
"   ‘gP’ line-wise -> beg. of first pasted line
" The builtin behavior is as follows; it is inconsistent and often annoying:
"  !‘p’ char-wise -> LAST pasted char
"   ‘p’ line-wise -> beg. of first pasted line
"   ‘gp’ char-wise -> after last pasted char
"   ‘gp’ line-wise -> beg. of line after last pasted line
"  !‘P’ char-wise -> LAST pasted char
"   ‘P’ line-wise -> beg. of first pasted line
"   ‘gP’ char-wise -> after last pasted char = initial position
"   ‘gP’ line-wise -> beg. of line after last pasted line = beg. of initial line
"
" These mappings have two caveats:
"   (1) don’t play well with the builtin ‘.’ command for repeating last action 
"       (it repeats the paste action proper but ignores subsequent movements); 
"       the plugin “repeat.vim” addresses that; the mappings below contain the 
"       plumbing to make them fully repeatable in case the plugin is installed 
"       (if not, the plumbing is simply ignored).
"   (2) the mapping for line-wise `p` requires that <Left> be allowed to move to 
"       the previous line; option 'whichwrap' must be set accordingly (done 
"       elsewhere in this vimrc).
nnoremap <silent> p p`[<Left>:silent! call repeat#set('p')<CR>
nnoremap <silent> gp gp`]:silent! call repeat#set('gp')<CR>
nnoremap P gP
nnoremap <silent> gP P`[:silent! call repeat#set('gP')<CR>

" A command to wrap two lines (overrides entering the annoying Ex Mode).
nnoremap Q gq2q
" Further disable every command that enters the Ex Mode, in order not to type it 
" by accident.
nnoremap gQ <Nop>
" Note: the command-line window opened by ‘q:’ is quite different and 
" occasionally useful, don’t disable it.
" https://vi.stackexchange.com/questions/457/does-ex-mode-have-any-practical-use

" A command to redo (overrides the weird undoing action which messes up the history).
nnoremap U <C-S-r>

" Allow left/right keys to move the cursor across line boundaries.
set whichwrap=b,s,<,>,[,]

" In Insert Mode, prevent the <Left>, <Right>, <Home> and <End> arrow keys from 
" breaking the undo sequence, and make insertion repeatable with "."
" (this wouldn’t work for <Up> and <Down> though). See `:help i_CTRL-G_U`.
inoremap <Left>  <C-g>U<Left>
inoremap <Right> <C-g>U<Right>
" Also, make <Home> alternate between column 1 and the first non-blank column. 
" (This code is drawn from Vim’s help but we fixed it for multibyte characters, 
" by replacing `col()` with `charcol()`; `match()` returns a byte count but, 
" luckily, here, we use it only to get the position of the 1st character that is 
" neither an ASCII space nor an ASCII tab (\S), so (assuming an ASCII-compatible 
" encoding) the result is also equal to the character count.)
inoremap <expr> <Home>
  \ charcol('.') == match(getline('.'), '\S') + 1 ?
  \   repeat('<C-g>U<Left>', charcol('.') - 1) :
  \   charcol('.') < match(getline('.'), '\S') ?
  \     repeat('<C-g>U<Right>', match(getline('.'), '\S') + 0) :
  \     repeat('<C-g>U<Left>', charcol('.') - 1 - match(getline('.'), '\S'))
inoremap <expr> <End> repeat('<C-g>U<Right>', charcol('$') - charcol('.'))

" Shuffle the meaning of motions ‘w, W, b, B’ for consistency, so that:
" - ‘w/W’ moves one word forward/backward;
" - ‘b/B’ moves one WORD forward/backward (mnemonic: “b” for “blob” or “block”);
" - ‘gw, gW, gb, gB’ are the inclusive or exclusive variants of ‘w, W, b, B’.
" Rationale:
" - The logic that lowercase/uppercase means forward/backward is consistent with 
"   many other commands (‘nN, tT, fF, xX, sS (our remapping of S), pP, gt/gT…’)
" - The logic of prefixing with ‘g’ is vaguely consistent with other things.
" - Motions with ‘g’ are used less often.
" - Motion ‘e’ had no symmetric counterpart.
"   Now, the easily accessible key ‘e’ is available for other things.
"noremap w w|  " unchanged
noremap W b
noremap b W
"noremap B B|  " unchanged
noremap gw e
noremap gW ge
noremap gb E
noremap gB gE
" also remap the related text objects for consistency
" (overrides the builtin ‘ib, ab, iB, aB’ which alias ‘i(, a(, i{, a{’ ):
onoremap iW iw
onoremap aW aw
onoremap ib iW
onoremap iB iW
onoremap ab aW
onoremap aB aW

" A command to go to last line (more consistent with ‘gg’ for first line).
noremap gG G

" Use ‘#, g#’ rather than ‘*, g*’ for searching the word under cursor
" (overrides builtin ‘#, g#’ which are the same but backwards).
" Rationale:
"   - searching backwards is confusing; just search forwards and ask for 
"     previous match with ‘N’;
"   - "#" is easier to type than "*" on the BÉPO layout;
"   - we want to map "*" to something else (see below).
noremap # *
noremap g# g*

" Motions ‘/, ÷, *, ×’ are like ‘t, f, T, F’ but they cross line boundaries, 
" work with arbitrary patterns, and require to press <Enter>
" (also, they pollute the search).
"   - ‘/’ is like ‘t’, forward and exclusive
"   - ‘÷’ is like ‘T’, backward and exclusive
"   - ‘*’ is like ‘f’, forward and inclusive
"   - ‘×’ is like ‘F’, backward and inclusive
" On BÉPO, "/, *" are next to each other, "÷" is AltGr+"/" and "×" is AltGr+"*". 
" Furthermore, with our custom langmap for BÉPO (see below), "/, *" are in the 
" same columns as "t, f".
"     https://vi.stackexchange.com/questions/4486/expand-f-and-t-motion-to-next-lines
"     https://www.reddit.com/r/vim/comments/fiq00/is_there_a_way_to_make_ttff_work_across_multiple/
" Implementation notes:
"   - The implementation uses the search feature of Vim.
"   - ‘/’ is just the builtin search command!
"   - We might be able to avoid polluting the search by using `:keeppatterns` 
"     like this: `:keeppatterns :exe "normal! /mypattern\<CR>"` but
"       (1) `:normal` wants a complete command, including the final Enter key, 
"           hence the necessary escaping with `:exe` and `\<CR>`;
"       (2) `:keeppatterns` does not seem to have any effect then (fixable bug?).
"     A better option might be to save and restore `@/` ?
"         https://stackoverflow.com/questions/52067483/vim-search-without-overwriting-register
"   - ‘÷’ has a bug, the character under cursor is included but shouldn’t
"     (looks like a bug in Vim).
onoremap ÷ ??e+1<Left><Left><Left><Left>
onoremap * //e<Left><Left>
onoremap × ?

" Make Shift+<left-arrow-like> and Shift+<right-arrow-like>
" behave like <Home> and <End> (more consistent)
" (overrides the builtin commands ‘H, L’ which are vertical movements).
noremap H ^
noremap L $
" Make ‘-’ and ‘+’ perform more interesting vertical movements
" (overrides the builtin commands ‘+’, ‘-’ which just move one line up/down).
noremap - H
noremap + L
" (Elsewhere in this vimrc, we map Shift+<up-arrow-like>, Shift+<down-arrow-like>
" to these interesting vertical movements.)

" Go to previous/next tab using Ctrl-Shift-Tab, Ctrl-Tab (only works in GVim):
"     https://stackoverflow.com/questions/1646819/how-can-i-map-ctrl-tab-to-something-in-vim
nnoremap <C-S-Tab> gT
inoremap <C-S-Tab> <C-o>gT
nnoremap <C-Tab> gt
inoremap <C-Tab> <C-o>gt

" Bindings for the BÉPO layout.
" TODO: go to previous/next tab, go to first/last tab:
"nnoremap gb gT
"nnoremap gé gt
"nnoremap <silent> gB :tabfirst<CR>
"nnoremap <silent> gÉ :tablast<CR>
" direct access to < and >:
noremap « <
noremap » >
" ignore NBSPs that a custom keyboard layout may insert around punctuation signs
" (otherwise, this would break commands such as >>):
noremap   <Nop>
noremap   <Nop>
" more bindings for the BÉPO layout (exchange é ↔ w, hjkl ↔ ctsr):
"source ~/.vim/vimrc_bépo_a3nm
" TODO: It seems better and simpler to use option 'langmap'.
" It is applied before command `:map`pings.
"
" IDEAS: replacement for hl,kj for left/right, up/down
"
" bearing in mind:
"   - HL are long vertical movements (not very useful: up/down screen)
"   - KJ are unrelated commands (useful: search keyword; join lines)
" also:
"   - ^$ are very useful and it would be nice to have them on Shift+Left/Right
" keys in our way to relocation:
"   - sS are useless commands
"   - rR are moderately useful commands
"   - nN, fF, tT, wW are very useful jump commands
"     + tT is also used in ‘gt, gT’
"     + hH is also used in ‘,h’ (custom command)
"   - m* are commands related to marks
"   - z* are commands related to folds and spellchecking
"
" Below are several options, from worst to best.
"
" <C-c> <C-r>, <C-s> <C-t>
"   => ??? see needed remappings
"   + same position as the QWERTY hlkj (familiarity)
"   − same position as the QWERTY hlkj (can’t make sense of it)
"   + can also be used in Insert Mode
"     (but then config is cumbersome, as 'langmap' doesn’t apply to Insert Mode)
"   − no shift combinations available?
"
" lj,k’ (uppercased: LJ,K?)
"   => (remap K -> ???)
"   => (remap ? -> ???)
"   + minimal change: no keys affected outside hjkl (and k is a fixpoint!)
"   − weird shape
"   − very hard to type (right = little finger; up, down = faraway keys)
"
" lj,wç
"   => remap wW -> ???
"   => (remap J -> ???)
"   + small change
"   + sensical shape
"   − hard to type (right, up, down = little finger)
"
" lj,hf
"   => remap fF -> JL
"   => remap J -> ???
"   better:
"   => remap fF -> sS
"   => abandon sS
"   + moderate change
"   + good mnemonic: L = “left”
"   + (fF->JL makes sense but is not consistent with tT)
"     fF->sS is consistent and is nicely located close to tT
"   ~ moderately sensical shape
"   ~ a bit hard to type (right, down = little finger)
"
" sn,lr
"   => abandon sS
"   => remap rR -> hH ?
"   => remap nN -> NS
"   + moderate change
"   ~ nN -> NS makes sense
"   + inverted T shape
"   ~ a bit hard to type (right = little finger)
"
" sj,lr
"   => abandon sS
"   => remap rR -> hH ?
"   + moderate change
"   + diagonal cross shape
"   − hard to type (right = little finger)
"
" tl,ds
"   => abandon sS
"   => remap dD -> ???
"   => remap tT -> hH ? (close to fF)
"   − large change, relocating common commands dD, tT (but l is a fixpoint!)
"   − where to put dD ?
"   + diagonal cross shape
"   + very easy to type
set langmap=
set langmap+=tlds;hlkj
" peculiarity: we translate "DS" to "-+" rather than "KJ"
" so as to separate the commands ‘K, J’ from the unrelated motions ‘k, j’:
"set langmap+=TLDS;HL-+
" the following combines the langmap with our custom command mappings for H, L; 
" it is highly peculiar, but we use it temporarily to circumvent some bugs (see 
" below):
set langmap+=TLDS;^$HL
" relocate t/T onto h/H:
set langmap+=hH;tT
" now we must relocate d/D;
" ‘d’ is an extremely frequent command, it must be on a strong finger,
" and it must combine nicely with motions, as in ‘dw, daw, d%, dt, df, d/’ …
" here is a suggestion:
" relocate d/D onto q/Q (very good position),
" relocate q onto k (‘q’ is a set of commands for macros, infrequently used;
" also used to quit as in <C-w>q, the new location is okay),
" relocate Q onto j (‘Q’ is a custom command for joining and wrapping lines,
" it fits nicely below ‘J’, a set of commands for joining lines)
set langmap+=qQ;dD
set langmap+=kj;qQ
" FIXME:
" the 'langmap' feature is seriously broken, when combined with `:map`…
" as soon as several consecutive keystrokes should be langmapped,
" the second keystroke skips either the langmap, or the :map.
" eg.  instead of  T → H → ^  we get  T → H               (:map is skipped)
"      instead of  D → - → H  we get  D → custom cmd ‘D’  (langmap is skipped)
"      instead of  S → + → L  we get  S → custom cmd ‘S’  (langmap is skipped)
"      instead of  Q → D → custom cmd ‘D’  we get  Q → custom cmd ‘Q’  (langmap is skipped)
"      instead of  j → Q → custom cmd ‘Q’  we get  j → Q  (:map is skipped)
" It’s clearly a bug in Vim, let’s report it and/or fix it!
" https://groups.google.com/g/vim_use/c/b7QB8U3xG_A
" https://groups.google.com/g/vim_dev/c/QnNwLWhJ744/m/1qNcD7d9OvgJ
" https://github.com/vim/vim/issues/297
" https://github.com/vim/vim/issues/7458
" https://stackoverflow.com/questions/12450803/vim-langmap-breaks-plugin-bépo
" https://github.com/neovim/neovim/issues/2420
" ...
" WORSE! 'langmap' is wreaking havoc in unrelated plugin key bindings,
" such as ‘u’ (repeat.vim) or ‘gcc’ (tcomment.vim) !
" disable it altogether:
set langmap=

" Navigation shortcuts.
" jump back to the previous tag location:
"nnoremap <BS> <C-T>
nnoremap Ç    <C-T>
" re‐jump to the next tag location (on the BÉPO layout, ç is close to <BS>):
nnoremap ç    :<C-U>exe v:count1 . 'tag'<CR>
" jump to the definition of the word under the cursor (needs a “tags” file):
"nnoremap <CR> <C-]>
" display the definition of the word under the cursor in the Preview Window 
" (needs a “tags” file):
nnoremap <silent> <LocalLeader>d :ptag <C-R><C-W><CR>
" display on the command bar the first line containing the word under the cursor 
" ignoring comments (hopefully, this will give the definition of the word):
nnoremap <LocalLeader>D [i

" Spell checking.
" commands to quickly toggle spell checking:
fun! ToggleSpellLang(lang)
  if &l:spelllang ==? a:lang
    setlocal spell!
  else
    setlocal spell
    let &l:spelllang = a:lang
  endif
endfun
command! EN :call ToggleSpellLang('en_us')
command! FR :call ToggleSpellLang('fr')
" go to previous/next misspelled word (BÉPO layout);
" the capitalized commands skip rare words:
nnoremap zb [s
nnoremap zB [S
nnoremap zé ]s
nnoremap zÉ ]S
" reminders:
" - z=: open spelling suggestions
" - zG/zg/2zg: add as a good word to the internal/local/user dictionary
" - zW/zw/2zw: add as a bad (wrong) word to the internal/local/user dictionary
" - zuG/zug/2zug / zuW/zuw/2zuw: undo, i.e. remove from a dictionary (buggy)

" Grammar checking (plugin using the standalone program “LanguageTool”).
" NOTE:
"   This requires LanguageTool to be installed somewhere, be it with ArchLinux’ 
"   package manager, or manually from one of:
"     https://languagetool.org/download/
"     https://github.com/languagetool-org/languagetool
"   In addition, one may also install the HUGE n-gram dataset to become able to 
"   detect more errors (confusion pairs like their/there):
"     https://dev.languagetool.org/finding-errors-using-n-gram-data
"   The up-to-date plugin for Vim is then found at:
"     https://github.com/dpelle/vim-LanguageTool
"   See `:help LanguageTool'. This plugin can be improved in several ways.
"     - It uses the XML output of LanguageTool (--api), which has been deprecated 
"       (since LanguageTool 3.5, Sept 2016) and replaced by a better (faster and 
"       maintained) JSON output (--json). This will break at some point.
"       See https://github.com/languagetool-org/languagetool/blob/master/languagetool-standalone/CHANGES.md
"     - It uses the standalone command-line tool, but LanguageTool can also run 
"       as a server. Using the server would cut the slow startup of the CLI tool 
"       (several seconds); plus, the Firefox addon is also able to use this local 
"       server, instead of sending all requests over the network.
"       See https://dev.languagetool.org/http-server
"   There is a fork with these 2 improvements (and more), but it targets NeoVim:
"     https://github.com/vigoux/LanguageTool.nvim
"   Other Vim plugins for LanguageTool:
"     https://github.com/rhysd/vim-grammarous
"     https://github.com/Konfekt/vim-langtool
"     ALE (alternative to Syntastic)
"   Review of these plugins:
"     https://jdhao.github.io/2020/09/20/nvim_grammar_check_languagetool/
let g:languagetool_cmd = '/usr/bin/languagetool'    " ArchLinux installation
"let g:languagetool_jar = '$HOME/LanguageTool-5.2/languagetool-commandline.jar'    " manual installation
command! Gram LanguageToolCheck
command! GramOff LanguageToolClear
hi link LanguageToolSpellingError SpellBad
hi link LanguageToolGrammarError  SpellCap

" Functions which extends the visual selection boundaries by a constant length 
" (with negative values, the selection is trimmed instead).
" Will not work as intended in the presence of multibyte characters.
"     https://vi.stackexchange.com/questions/16996/expand-the-visual-selection-independently-of-the-cursor-position
fun! ExpandVisualBefore(by_lines, by_cols)
  let [start_buf, start_line, start_col, start_off] = getpos("'<")
  let end_pos                                       = getpos("'>")
  if start_off != 0
    throw 'function ExpandVisualBefore(): ''virtualedit'' is not supported'
  endif
  " Note: “:help getpos()” says that the column of '< should be 0 when in 
  " Visual‐Line Mode, but my tests indicate that it is set to 1 instead.
  " the column of '> should be set to a “large number”, which my tests confirm.
  if end_pos[2] >= 2147483647 && a:by_cols != 0
    echohl WarningMsg
    echo 'function ExpandVisualBefore(): cannot add or remove columns in visual‐line mode'
    echohl None
  endif
  if setpos("'<", [start_buf, start_line-a:by_lines, start_col-a:by_cols, 0]) == -1
    throw 'function ExpandVisualBefore(): setpos() failed'
  endif
  " Note: For some obscure reason, when the cursor is at the start of the 
  " selection, changing '< apparently sets '> to the former value of '<. Below, 
  " we fix that by restoring '> to its initial value.
  " Under the same circumstances, the cursor is moved to the end of the 
  " selection, we should fix that as well…
  if setpos("'>", end_pos) == -1
    throw 'function ExpandVisualAfter(): setpos() failed'
  endif
endfun
fun! ExpandVisualAfter(by_lines, by_cols)
  let start_pos                             = getpos("'<")
  let [end_buf, end_line, end_col, end_off] = getpos("'>")
  if end_off != 0
    throw 'function ExpandVisualAfter(): ''virtualedit'' is not supported'
  endif
  if end_col >= 2147483647 && a:by_cols != 0
    echohl WarningMsg
    echo 'function ExpandVisualAfter(): cannot add or remove columns in visual‐line mode'
    echohl None
  endif
  if setpos("'>", [end_buf, end_line+a:by_lines, end_col+a:by_cols, 0]) == -1
    throw 'function ExpandVisualAfter(): setpos() failed'
  endif
  " Note: For some obscure reason, when the cursor is at the start of the 
  " selection, changing '> apparently sets '< to the former value of '>. Below, 
  " we fix that by restoring '< to its initial value.
  " Under the same circumstances, the cursor is moved to the end of the 
  " selection, we should fix that as well…
  if setpos("'<", start_pos) == -1
    throw 'function ExpandVisualAfter(): setpos() failed'
  endif
endfun
" Example use: 3+ (resp. 3\+) adds three characters at the beginning (resp. end) 
" of the selection.
"     http://vim.wikia.com/wiki/Mapping_keys_in_Vim_-_Tutorial_(Part_2)#Supplying_a_count_to_a_map
xnoremap <silent> \- :call ExpandVisualBefore(0,-v:count1)<CR>gv
xnoremap <silent> \+ :call ExpandVisualBefore(0,v:count1)<CR>gv
xnoremap <silent> - :call ExpandVisualAfter(0,-v:count1)<CR>gv
xnoremap <silent> + :call ExpandVisualAfter(0,v:count1)<CR>gv

" Smart pairs.
" An opening parenthesis / bracket / curly brace inserts the closing character 
" automatically (<C-g>U prevents <Left> from breaking the undo sequence).
" See `:help i_CTRL-G_U` and
" https://vim.fandom.com/wiki/Automatically_append_closing_characters
" Note that the paste mode disables these smart characters.
inoremap ( ()<C-g>U<Left>
inoremap [ []<C-g>U<Left>
inoremap { {}<C-g>U<Left>
" Ditto for ASCII quotes and curly quotes.
inoremap " ""<C-g>U<Left>
inoremap ' ''<C-g>U<Left>
inoremap “ “”<C-g>U<Left>
inoremap ‘ ‘’<C-g>U<Left>
" Ditto for guillemots (with or without spaces within).
inoremap «        «»<C-g>U<Left>
inoremap «        «  »<C-g>U<Left><C-g>U<Left>
inoremap «<Space> «  »<C-g>U<Left><C-g>U<Left>
inoremap ‹        ‹›<Left>
inoremap ‹        ‹  ›<C-g>U<Left><C-g>U<Left>
inoremap ‹<Space> ‹  ›<C-g>U<Left><C-g>U<Left>
" Each closing character just skips over the next character if it is the 
" intended one.
inoremap <expr> ) strpart(getline('.'), col('.')-1, 1, v:true) ==# ')' ? '<C-g>U<Right>' : ')'
inoremap <expr> ] strpart(getline('.'), col('.')-1, 1, v:true) ==# ']' ? '<C-g>U<Right>' : ']'
inoremap <expr> } strpart(getline('.'), col('.')-1, 1, v:true) ==# '}' ? '<C-g>U<Right>' : '}'
inoremap <expr> ” strpart(getline('.'), col('.')-1, 1, v:true) ==# '”' ? '<C-g>U<Right>' : '”'
inoremap <expr> ’ strpart(getline('.'), col('.')-1, 1, v:true) ==# '’' ? '<C-g>U<Right>' : '’'
" For ASCII quotes the logic is slightly different, because the opening and 
" closing characters are the same.
inoremap <expr> " strpart(getline('.'), col('.')-1, 1, v:true) ==# '"' ? '<C-g>U<Right>' : '""<C-g>U<Left>'
inoremap <expr> ' strpart(getline('.'), col('.')-1, 1, v:true) ==# "'" ? '<C-g>U<Right>' : "''<C-g>U<Left>"
" For guillemots, since 
inoremap <expr> »
  \ strpart(getline('.'), col('.')-1, 1, v:true) ==# '»' ? '<C-g>U<Right>' :
  \ strpart(getline('.'), col('.')-1, 2, v:true) =~# '[   ]»' ? '<C-g>U<Right><C-g>U<Right>' : '»'
inoremap <expr> ›
  \ strpart(getline('.'), col('.')-1, 1, v:true) ==# '›' ? '<C-g>U<Right>' :
  \ strpart(getline('.'), col('.')-1, 2, v:true) =~# '[   ]›' ? '<C-g>U<Right><C-g>U<Right>' : '›'

" ( in Visual Mode inserts parentheses around the selection, ) deletes them.
"xnoremap ( <Esc>`>a)<Esc>`<i(<Esc>:call ExpandVisualAfter(0,2)<CR>gv
"xnoremap ) <Esc>`>x`<x:call ExpandVisualAfter(0,-2)<CR>gv
" Largely subsumed by the plugin “surround”.

" plugin “surround”: targets “q” and “Q” are double and simple curly quotes:
let g:surround_{char2nr('q')} = "“\r”"
let g:surround_{char2nr('“')} = "“\r”"
let g:surround_{char2nr('”')} = "“\r”"
let g:surround_{char2nr('Q')} = "‘\r’"
let g:surround_{char2nr('‘')} = "‘\r’"
let g:surround_{char2nr('’')} = "‘\r’"
" plugin “surround”: targets “g” and “G” are French guillemots with and without spaces:
let g:surround_{char2nr('g')} = "« \r »"
let g:surround_{char2nr('«')} = "« \r »"
let g:surround_{char2nr('G')} = "«\r»"
let g:surround_{char2nr('»')} = "«\r»"
" plugin “surround”: targets “h” and “H” are Swiss chevrons with and without spaces:
let g:surround_{char2nr('h')} = "‹ \r ›"
let g:surround_{char2nr('‹')} = "‹ \r ›"
let g:surround_{char2nr('H')} = "‹\r›"
let g:surround_{char2nr('›')} = "‹\r›"

" Load builtin package “matchit”, which makes % jump between language‐dependent 
" delimiters (for example, XML tags), in addition to parentheses.
packadd! matchit

" This function allows to define Insert‐Mode abbreviations that are substituted 
" as soon as we type a bang after them. For example:
"     :iabbrev <expr> paste BangAbbrev('paste', "\<C-R>+")
" or, using the shortcut function:
"     :call MakeBangAbbrev('paste', "\<C-R>+")
" Then, any sequence “paste!” typed in Insert Mode gets replaced with the 
" contents of the clipboard; “paste” not followed by a bang is left as is.
" The function MakeBangAbbrev() takes an optional third parameter, which 
" indicates whether the abbreviation should be made global (1) or local (0).
"     https://vi.stackexchange.com/questions/16998/substitute-an-insert-mode-input-immediately
" NOTE:
"   We don’t use this anymore. Templates are now provided via the plugin 
"   “UltiSnips”, which is much better. Filetype-independent snippets are in 
"   ~/.vim/UltiSnips/all.snippets .
"fun! BangAbbrev(abbrev, subst)
"  " FIXME: :iabbrev <expr> already provides v:char, so getchar() is not needed?
"  " (:help map-expression)
"  let c = nr2char(getchar(0))
"  if c ==# '!'
"    let s = a:subst
"  else
"    let s = a:abbrev . c
"  end
"  " make sure the sequence is run in paste mode, to avoid characters being 
"  " interpreted further by custom key mappings (resetting 'nopaste' afterwards 
"  " is safe, because the abbreviation would not have been interpreted in paste 
"  " mode anyway):
"  "return "\<C-o>:set paste\<CR>" . s . "\<C-o>:set nopaste\<CR>"
"  " we cannot do that, because in many cases we rely on automatic indentation.
"  return s
"endfun
"fun! MakeBangAbbrev(abbrev, subst, ...)
"  " read the optional third argument:
"  let globally = get(a:, 1, 1)
"  let global_flag = globally ? '' : '<buffer>'
"  " quote values for metaprogramming with :execute :
"  " FIXME:
"  "   there should be another level of quoting, because of :abbrev special 
"  "   syntax tricks: :abbrev, like :map, recognizes key symbols such as '<Left>' 
"  "   (so that writing "\<Left>" is unnecessary (buggy?).
"  "     :help map-special-keys
"  "   moreover, a bar | has to be escaped, otherwise it terminates the mapping.
"  "     :help map-bar
"  "   vimscript really is a quoting Hell.
"  let abbrev = string(a:abbrev)
"  let subst = string(a:subst)
"  " register the abbreviation:
"  exe 'inoreabbrev' global_flag '<silent> <expr>' a:abbrev 'BangAbbrev(' abbrev ',' subst ')'
"endfun
"
"" Example: shortcut to write a shebang.
""call MakeBangAbbrev('shebang', '#!/usr/bin/')

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins                                                                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Load the bundles (add the directories under ~/.vim/bundle to the runtimepath).
execute pathogen#infect()

function! s:check_defined(variable, default)
  if !exists(a:variable)
    let {a:variable} = a:default
  endif
endfunction

" Airline theme.
let g:airline_theme = 'serene'
" disable Powerline private symbols and Unicode characters:
let g:airline_symbols_ascii = 1
" customize separators:
let g:airline_left_sep = ''
let g:airline_left_alt_sep = '»'
let g:airline_right_sep = ''
let g:airline_right_alt_sep = '«'
let g:airline_skip_empty_sections = 1
" customize the display of the mode, so that it is generally shorter:
call s:check_defined('g:airline_mode_map', {})
call extend(g:airline_mode_map, {
  \ '__' : '---',
  \ 'n'  : '   ',
  \ 'i'  : 'INS',
  \ 'R'  : 'REP',
  \ 'v'  : 'VIS',
  \ 'V'  : 'VIS-LINE',
  \ '' : 'VIS-BLOCK',
  \ 's'  : 'SEL',
  \ 'S'  : 'SEL-LINE',
  \ '' : 'SEL-BLOCK',
  \ 'c'  : 'CMD',
  \ 't'  : 'TERM',
  \ }, 'force')
" customize the display of the paste-mode and spell-checking indicators
" (defaults: 'PASTE' and 'SPELL'):
call s:check_defined('g:airline_symbols', {})
if $TERM ==? 'linux'
  " if running in a TTY, use characters that my TTY is able to display:
  let g:airline_symbols.paste = '¶'
  let g:airline_symbols.spell = 'Š'
else
  " funny chars: s̰šč (U+330 tilde below)
  let g:airline_symbols.paste = '¶'
  let g:airline_symbols.spell = 'S̰'
endif
" display a short path (???):
"let g:airline_stl_path_style = 'short'
"let g:airline_section_c_only_filename = 1
" customize the display of the file encoding information
" (see also `g:airline#parts#ffenc#skip_expected_string`):
function! Airline_part_ffenc_custom()
  let fenc = &fenc ==? 'utf-8' ? '' : &fenc
  let bomb = &l:bomb ? 'bom' : ''
  let ff =
  \ strlen(&ff) ? get({
  \   'unix' : '',
  \   'dos'  : 'crlf',
  \   'mac'  : 'cr',
  \ }, &ff) : ''
  let tmp = join(filter([ bomb, ff ], { i, s -> !empty(s) }), '+')
  return strlen(tmp) ? fenc . '/' . tmp : fenc
endfunction
call airline#parts#define_function('ffenc', 'Airline_part_ffenc_custom')
" customize the display of word count:
let g:airline#extensions#wordcount#formatter#default#fmt = '%sw'    " default: '%s words'
let g:airline#extensions#wordcount#formatter#default#fmt_short = '%sw'
" enable search count:
let g:airline#extensions#searchcount#enabled = 1
" customize the display of line / column numbers (the default wastes a lot of 
" space and is pretty messy, especially when fancy characters are unavailable):
call airline#parts#define_raw('perc', '%2p%%')
call airline#parts#define_raw('linecol', '%3l:%2v')
" formatting of the various pieces:
call airline#parts#define_accent('file', 'bold')
call airline#parts#define_accent('path', 'bold')
call airline#parts#define_accent('readonly', 'orange')
call airline#parts#define_accent('filetype', 'orange')
call airline#parts#define_accent('ffenc', 'red')
call airline#parts#define_accent('linecol', 'bold')
let g:airline_section_z = airline#section#create(['perc', ' ', 'linecol'])
" disable all whitespace-related warnings in Airline:
"let g:airline#extensions#whitespace#enabled = 0
" in Markdown files, disable warnings about trailing spaces and long lines, 
" since I often use fo+=w and the trailing space can run past the column limit:
au FileType markdown let b:airline_whitespace_checks =
  \  [ 'indent', 'mixed-indent-file', 'conflicts' ]
" more lenient detection of mixed indent (tolerate spaces after tabs):
let g:airline#extensions#whitespace#mixed_indent_algo = 1
" customize the display of whitespace-related warnings:
let g:airline#extensions#whitespace#symbol = ''
let g:airline#extensions#whitespace#trailing_format          = '%s:trailing ⍽'
let g:airline#extensions#whitespace#mixed_indent_format      = '%s:mixed \t'
let g:airline#extensions#whitespace#mixed_indent_file_format = '%s:mixed \t across file'
let g:airline#extensions#whitespace#long_format              = '%s:long line'
let g:airline#extensions#whitespace#conflicts_format         = '%s:conflict'
" disable Syntastic integration in Airline:
"let g:airline#extensions#syntastic#enabled = 0

" Syntastic settings.
let g:syntastic_check_on_open = 1
let g:syntastic_auto_jump = 0
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_loc_list_height = 5
let g:syntastic_quiet_messages = {'level': [], 'type' : 'style'}
if $TERM ==? 'linux'
  let g:syntastic_error_symbol = 'E'
  let g:syntastic_warning_symbol = 'W'
  let g:syntastic_style_error_symbol = 'e~'
  let g:syntastic_style_warning_symbol = 'w~'
else
  let g:syntastic_error_symbol = '✗'
  let g:syntastic_warning_symbol = '⚠'
  let g:syntastic_style_error_symbol = '✗~'
  let g:syntastic_style_warning_symbol = '⚠~'
endif
" vim-airline until version 0.8 honoured g:syntastic_stl_format; but starting 
" with version 0.9, it has a separate zone for errors and for warnings:
"let g:syntastic_stl_format = '%E{%eE (→%fe)}%B{ }%W{%wW (→%fw)}'
let airline#extensions#syntastic#stl_format_err  = '%E{%e (→%fe)}'
let airline#extensions#syntastic#stl_format_warn = '%W{%w (→%fw)}'
" Syntax groups for error reporting include ErrorMsg / WarningMsg (for errors of 
" Vim itself), Error (for syntax errors—but there is no Warning counterpart, 
" Todo is commonly used instead) and SpellBad / SpellCap (for spellchecking). 
" Default values for Syntastic are not consistent, so we fix it here.
" default: SpellBad / SpellCap
hi link SyntasticError       ErrorMsg
hi link SyntasticWarning     WarningMsg
" default: Error / Todo
hi link SyntasticErrorSign   ErrorMsg
hi link SyntasticWarningSign WarningMsg

" UltiSnips settings.
let g:UltiSnipsExpandTrigger = '<Tab>'
let g:UltiSnipsJumpForwardTrigger = '<Tab>'
let g:UltiSnipsJumpBackwardTrigger = '<C-b>'
let g:UltiSnipsEditSplit = 'vertical'
" let g:UltiSnipsJumpForwardTrigger = '<C-b>'
" let g:UltiSnipsExpandTrigger = '<C-b>'

" TComment settings.
" similarly to ‘gcc’ (Normal Mode) and ‘gc’ (Visual Mode) for line comments, 
" ‘gbb’ (Normal Mode) and ‘gb’ (Visual Mode) toggle block comments (for some 
" reason, ‘gcb’ does not work):
"nnoremap gbb  :TCommentBlock<CR>
"xnoremap gb   :TCommentBlock<CR>
" rather use ‘gCC’ and ‘gC’, because elsewhere in this vimrc we use ‘gb’ for 
" other things:
nnoremap gCC  :TCommentBlock<CR>
xnoremap gC   :TCommentBlock<CR>
" by default, do not add spaces around comment delimiters (by convention, we do 
" not add spaces when commenting out portions of code, so as to distinguish it 
" from actual, textual comments):
call extend(g:tcomment#options, { 'whitespace' : 'no' })

" LaTeX-Unicoder settings.
" disable the keyboard binding in Normal Mode (useless):
let g:unicoder_cancel_normal = 1
" add custom symbol names:
" - use prettier variants (epsilon, phi, empty sets)
" - define more logical symbols
let g:unicode_map =
\ {
\   "\\empty" : '∅',
\   "\\emptyset" : '∅',
\   "\\nothing" : '∅',
\   "\\epsilon" : 'ε',
\   "\\phi" : 'φ',
\   "\\defeq" : '≙',
\   "\\eqdef" : '≙',
\   "\\models"  : '⊨',
\   "\\entails" : '⊢',
\   "\\limplies" : '→',
\   "\\biglor" :  '⋁',
\   "\\bigland" : '⋀',
\   "\\sep" : '∗',
\   "\\wand" : '-∗',
\   "\\box" : '□',
\ }

" Unicode.vim plugin: make `ga` show more info about the glyph under cursor, 
" including the code points it is made of (in case of combining diacritics) 
" and their names:
nnoremap <silent> ga <Plug>(UnicodeGA)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Filetype-specific settings                                                   "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""
""" Vim help

"     http://vim.wikia.com/wiki/Learn_to_use_help
" simplify navigation into Vim help (<C-]> is incredibly painful to type):
au Filetype help call SetBetterNavBindings()
fun! SetBetterNavBindings()
  nnoremap <buffer> <CR> <C-]>
  nnoremap <buffer> <BS> <C-T>
endfun
" Remainder: use `:helptags ALL` to regenerate all help tags.

""""""""""""""""""""
""" Vimscript

" indentation rules:
au FileType vim setlocal expandtab shiftwidth=2
" deactivate the automatic insertion of a closing double quote:
au FileType vim inoremap <buffer> " "

""""""""""""""""""""
""" Markdown

" indentation rules:
au FileType markdown setlocal expandtab shiftwidth=4
" better auto‐formatting:
au FileType markdown setlocal formatoptions+=rota
" add support for ZdS syntax extension:
au FileType markdown let &comments = &comments . ',n:|'
" add support for YAML metadata header (dimmed, as comments):
let g:markdown_yaml_head = 1
hi def link yamlPlainScalar Comment
" simpler highlighting, without a full-fledged YAML highlighting:
"au FileType markdown call SetCustomMarkdownSyntax()
"fun! SetCustomMarkdownSyntax()
"  syn region markdownYaml matchgroup=markdownYamlDelim
"    \ start='\%1l^---$' end='^\%(---\|\.\.\.\)$'
"    \ contains=@Spell,markdownYamlKey
"  syn match markdownYamlKey '^[^:]\+:' contained contains=@NoSpell
"  hi def link markdownYamlDelim PreProc
"  hi def link markdownYaml      Comment
"  hi def link markdownYamlKey   Identifier
"endfun
au FileType markdown call MarkdownSettings()
fun! MarkdownSettings()
  " plugin “surround”: targets “i” and “I” are Markdown *italic* and **bold** delimiters:
  let b:surround_{char2nr('i')} = "*\r*"
  let b:surround_{char2nr('I')} = "**\r**"
endfun

""""""""""""""""""""
""" Wikicode

" Unfortunately there does not seem to exist a filetype for Wikicode text.
" plugin “surround”: targets “i” and “I” are Wikicode ''italic'' and '''bold''' delimiters:
let g:surround_{char2nr('i')} = "''\r''"
let g:surround_{char2nr('I')} = "'''\r'''"
" plugin “surround”: targets “w” and “W” are Wikicode [[links]] and {{templates}}:
let g:surround_{char2nr('w')} = "[[\r]]"
let g:surround_{char2nr('W')} = "{{\r}}"
" plugin “surround”: targets “L” and “T” are similar to the previous,
" but they prompt for the link target or template name (experimental):
let g:surround_{char2nr('L')} = "[[\1wiki link: \1|\r]]"
let g:surround_{char2nr('T')} = "{{\1wiki template: \1|\r}}"

""""""""""""""""""""
""" LaTeX

" TODO:
"   - prevent closing brackets from breaking current indent
"   - more intelligent closing brackets supporting \{\}, \[\], \(\) \left.\right.
"   - fix formatoptions (fo-=a ?)
"   - keep textwidth to 80 (sometimes it is reset to 0, dunno how)
" use LaTeX as the default TeX format (instead of Plain TeX):
let g:tex_flavor = 'latex'
" indentation rules:
au FileType tex setlocal expandtab shiftwidth=2
" disable automatic indentation from plugin “vimtex”, re-enable default plugin 
" (vimtex insists on re-indenting the current line when an opening delimiter 
" like '{' or '[' or '(' is inserted):
let g:vimtex_indent_enabled = 0
" deactivate the automatic insertion of a closing brace:
"au FileType tex inoremap <buffer> { {
" enable code folding… https://stackoverflow.com/a/72584993
" … using the builtin plugin:
"let g:tex_fold_enabled = 1
"au FileType tex :syntax clear texDocZone " don’t fold \begin{document} (major slowdown, see URL above)
" … OR using VimTeX, which is better and faster:
unlet! g:tex_fold_enabled    " make sure the builtin TeX folding is disabled
let g:vimtex_fold_enabled = 1
" configure VimTeX folding:
" - fold the preamble
" - fold \sectionframe and \paragraph as sectioning commands
" - fold Vim-style markers, i.e. {{{ and }}} in comments
" - don’t fold comments
" - don’t fold list environments
let g:vimtex_fold_types =
\ {
  \ 'preamble' : { 'enabled' : 1 },
  \ 'sections' : { 'enabled' : 1,
    \ 'sections' : ['part', 'chapter', 'sectionframe', 'section',
    \               'subsection', 'subsubsection', 'paragraph'] },
  \ 'markers'  : { 'enabled' : 1 },
  \ 'comments' : { 'enabled' : 0 },
  \ 'envs' : { 'blacklist' : ['itemize', 'enumerate'] }
\ }
" disable spellchecking in comments (because my comments often contain code or 
" foreign-language text):
let g:tex_comment_nospell = 1
" disable intrusive Syntastic features because LaTeX checkers are too strict:
au FileType tex :let b:syntastic_auto_loc_list = 0 |let b:syntastic_auto_jump = 0
" disable some error messages:
let g:syntastic_tex_lacheck_quiet_messages =
\ { 'regex' : [
    \ '^-> unmatched',
    \ '^<- unmatched',
    \ '^Do not use @',
    \ "^Don't use ".'"\\cr"',
\ ] }
au BufRead,BufNewFile *.cls,*.sty :call TeXIgnoreMoreErrors()
fun! TeXIgnoreMoreErrors()
  let ignore_more =
  \ [
      \ "^Don't use ".'".*" in LaTeX documents',
  \ ]
  let d = copy(g:syntastic_tex_lacheck_quiet_messages)
  let d['regex'] = (has_key(d, 'regex') ? d['regex'] : [ ]) + ignore_more
  let b:syntastic_tex_lacheck_quiet_messages = d
endfun
" worse: disable Syntastic altogether:
"au FileType tex :let b:syntastic_mode = 'passive'
"au FileType tex :let b:syntastic_skip_checks = 1
" a key binding for compiling quickly (ugly):
"au FileType tex :nnoremap <buffer> ,, :w \| !pdflatex % && bibtex %:r && pdflatex %<CR>
" third‐party plugins like “vimtex” do this much better (using “latexmk”) and 
" they even support automatic recompilation and PDF previewing.

" vimtex’s default config for latexmkrc uses the option 
" '-interaction=nonstopmode', which ignores all errors and produces a PDF 
" anyway; the following SHOULD override the option, and '-f' makes sure that 
" errors are not ignored, but for some reason vimtex ignores this setting on 
" most of my existing files…
let g:vimtex_compiler_latexmk = { 'options': [ '-verbose', '-file-line-error', '-synctex=1', '-f-' ] }

""""""""""""""""""""
""" Shell

" highlight shell scripts as per POSIX, not the very old original Bourne shell:
let g:is_posix = 1
" Our shell code looks like a scheme programmer made up all the names
autocmd FileType sh setlocal iskeyword=~,@,48-57,_,192-255,-

""""""""""""""""""""
""" Scala

" use the ‘scalac’ compiler for syntax cheking instead of the default ‘fsc’ 
" (which in practice seems even more resource‐consuming and not faster (!)):
let g:syntastic_scala_checkers = ['scalac']

""""""""""""""""""""
""" OCaml

" Summary of plugins:
"   — There is a default, undocumented OCaml plugin shipped with Vim
"     ( $VIMRUNTIME/ftplugin/ocaml.vim )
"     It is outdated. The up‐to‐date versions of Vim files for OCaml are 
"     maintained by the community there:
"     ( https://github.com/ocaml/vim-ocaml/ )
"     We use the latter, as a regular bundle (git clone in ~/.vim/bundle/ ; 
"     files there apparently override the default files in $VIM).
"     It provides:
"       + filetype detection
"       + syntax highlighting
"       + code folding (slow and totally broken)
"       + configuration for the optional package “matchit” (which makes % jump 
"         between pairs of keywords, for example while → do → done)
"       + compiler settings for Quickfix (vasistas?)
"       + an :Opam command to switch compiler versions from within Vim
"       + an :Ocpgrep command for interrogating *.cm{i,ti,t} files (uses the 
"         command‐line program “ocp-grep” from opam package “ocp-index”)
"       + <LocalLeader>c : comment a line or a block
"       + <LocalLeader>C : uncomment a line or a block
"       + <LocalLeader>s : switch between Mod.ml and Mod.mli files
"       + <LocalLeader>S : ditto in a new window
"       + <LocalLeader>t : show the type of an expression (uses Mod.annot files, 
"                          overridden by Merlin)
"   — There is a plugin called “Merlin”, which serves as an IDE for OCaml: it 
"     provides type/error checking integrated with Syntastic, auto‐completion, 
"     jump‐to‐definition, documentation previewing, scope‐aware renaming…
"     It is available as an opam package.
"   — There is a plugin called “ocp‐indent” for indentation.
"     It is available as an opam package.
"     Configuration goes in ~/.ocp/ocp-indent.conf .
"   — We use snippets defined in ~/.vim/{snippets,UltiSnips}/ocaml.snippets , 
"     whose support is provided by the plugin “UltiSnips”.
"     It is available as a regular bundle.
"   — We use comment commands provided by the plugin “tcomment”.
"     It is available as a regular bundle.

let g:opamshare=substitute(system('opam var share'),'\n$','','')

" indentation rules:
au FileType ocaml setlocal expandtab shiftwidth=2
" comment settings (provided by plugin “tcomment”); by convention, we use
" (*! … !*), with spaces, to comment out portions of code; textual comments are 
" written as (* … *), ocamldoc comments are written as (** … *):
call tcomment#type#Define('ocaml',        '(*! %s !*)')
call tcomment#type#Define('ocaml_inline', '(*! %s !*)')
call tcomment#type#Define('ocaml_block',  "(*!!!!!!%s  !!!!!!*)\n  ! ")
" enable code folding (provided by the default plugin; slow and totally broken):
"let g:ocaml_folding = 1
" enable highlighting of operators:
let g:ocaml_highlight_operators = 1
" plugin “ocp-indent” (tool for indenting):
execute 'set rtp^=' . g:opamshare . '/ocp-indent/vim'
" plugin “merlin” (IDE for OCaml):
execute 'set rtp^=' . g:opamshare . '/merlin/vim'
" make Syntastic use Merlin for error detection:
let g:syntastic_ocaml_checkers = ['merlin']
" disable some error messages
" (these are a bug of Merlin, fixed in 5.2):
let g:syntastic_ocaml_merlin_quiet_messages =
\ { 'regex' : [
    \ '^unknown flag -unboxed-types$',
    \ '^unknown flag -no-unboxed-types$',
\ ] }
" text objects which select OCaml expressions (m / im / am grow the selection, 
" M / iM / aM shrink it):
let g:merlin_textobject_grow   = 'm'
let g:merlin_textobject_shrink = 'M'
" Merlin uses this style to highlight an expression when showing its type (we 
" set a custom background color so as to distinguish it from search results):
hi EnclosingExpr ctermbg=17 guibg=#00005f
" local settings (including Insert‐Mode abbreviations):
au FileType ocaml call OCamlSettings()
fun! OCamlSettings()
  " indent and align pattern matching under the cursor (see below):
  nnoremap <buffer> <LocalLeader>m  :call OCamlAlignPatternMatchingArrows()<CR>
  " search all occurrences of the identifier under the cursor (similar to * / #, 
  " but specialized for OCaml identifiers):
  nmap <buffer> <LocalLeader>*  <Plug>(MerlinSearchOccurrencesForward)
  nmap <buffer> <LocalLeader>#  <Plug>(MerlinSearchOccurrencesBackward)
  " rename an identifier, interactively:
  nmap <buffer> <LocalLeader>r  <Plug>(MerlinRename)
  nmap <buffer> <LocalLeader>R  <Plug>(MerlinRenameAppend)
  " jump to the definition of an identifier (we override our global command, 
  " which used tags, because for OCaml, Merlin does it better):
  nnoremap <buffer> <LocalLeader>d  :MerlinLocate<CR>
  " show the documentation of an identifier (override a global command):
  nnoremap <buffer> <LocalLeader>D  :MerlinDocument<CR>
  " the default value of option 'comments' (set by ocp-indent):
  "setlocal comments=sr:(*,mb:*,ex:*)
  " a more sensible value:
  "   — simple comments are handled only when the opening delimiter is followed 
  "     by a space, so that it does not apply to ocamldoc comments — which start 
  "     with (** – nor to commented out code — for which i put no extra space, 
  "     by convention. hence, in those cases, no star is automatically inserted.
  "   — ocamldoc comments are handled separately, so that line wrapping gives 
  "     proper alignment; moreover, we can avoid getting a star inserted at the 
  "     beginning of each intermediate line of the doc‐comment (if we do not use 
  "     option -stars of ocamldoc) [but in that case, alignment only works as 
  "     long as there is at least one character typed on the first line].
  "   — commented code, following my convention with (*! … !*), is handled too.
  setlocal comments=sr:(*\ ,mb:*,ex:*)      " simple comments
  "setlocal comments^=sr:(**,mb:*,ex:*)      " ocamldoc comments with -stars
  setlocal comments^=sr:(**,mb:\ ,ex:*)     " ocamldoc comments without -stars
  setlocal comments^=sr:(*!,mb:\ !,ex:!*)   " commented code
  " TODO: not sure what the effect of this is, but it sounds relevant:
  setlocal commentstring=(*%s*)
  " for OCaml code, we restore TComment’s default behaviour regarding spaces:
  call s:check_defined('b:tcomment_options', {})
  call extend(b:tcomment_options, { 'whitespace' : '' })
  " deactivate the automatic insertion of a closing simple quote:
  inoremap <buffer> ' '
  " plugin “surround”: targets “c” and “C” are OCaml comments with or without 
  " spaces:
  let b:surround_99 = "(* \r *)"
  let b:surround_67 = "(*\r*)"
  " plugin “surround”: the target “A” is an OCaml array delimiter:
  let b:surround_65 = "[| \r |]"
  " plugin “surround”: the target “B” is a begin…end block (shadows the builtin 
  " target for curly braces {}, but these are less useful for OCaml anyways):
  let b:surround_66 = "begin \r end"
  " templates are now provided via the plugin “UltiSnips”, they are stored in 
  " ~/.vim/snippets/ocaml.snippets .
"  " type “c!” to get a “(* · *)” template:
"  " enter paste mode so that no closing parenthesis is automatically inserted; 
"  " stay in paste mode as this may be relevant for writing comments (for 
"  " example, it disables all the previous shortcuts):
"  call MakeBangAbbrev('c', '<C-o>:set paste<CR>(*  *)<C-g>U<Left><C-g>U<Left><C-g>U<Left>', 0)
"  " type “C!” to get a “(** · *)” template (same remark regarding paste mode):
"  call MakeBangAbbrev('C', '<C-o>:set paste<CR>(**  *)<C-g>U<Left><C-g>U<Left><C-g>U<Left>', 0)
"  " type “a!” to get a “[| · |]” template (I can’t type this by hand!):
"  call MakeBangAbbrev('a', '<C-o>:set paste<CR>[\|  \|]<C-g>U<Left><C-g>U<Left><C-g>U<Left><C-o>:set nopaste<CR>', 0)
"  " type “b!” to get a vertical “begin · end” template:
"  call MakeBangAbbrev('b', 'begin<CR>X<BS><CR>end<Up>', 0)
"  " type “d!” to get a vertical “do · done” template:
"  call MakeBangAbbrev('d', 'do<CR>X<BS><CR>done<Up>', 0)
"  " type “m!” to get a vertical “begin match · with  end” template:
"  call MakeBangAbbrev('m', 'begin match  with<CR>\|<CR>end<Up><Up><C-o>$<C-o>F ', 0)
"  " type “t!” to get a vertical “begin try · with _ ->  end” template:
"  call MakeBangAbbrev('t', "begin try\<CR>X\<BS>\<CR>with _ ->\<CR>X\<BS>\<CR>end\<Up>\<Up>\<Up>", 0)
"  " type “f!” to get a template for a for-loop with breaks:
"  call MakeBangAbbrev('f', 'begin try for  do<CR><C-d>X<CR>done with Break -><CR>X<BS><CR>end<Up><Up><Up><BS><Up><C-o>$<C-o>F ', 0)
"  " type “f!” to get a template for a while-loop with breaks:
"  call MakeBangAbbrev('w', 'begin try while  do<CR><C-d>X<CR>done with Break -><CR>X<BS><CR>end<Up><Up><Up><BS><Up><C-o>$<C-o>F ', 0)
"  " most of these templates rely on some plugin providing automatic indentation 
"  " for OCaml; the sequence “X<BS>” (insert X then deletes it) is often used to 
"  " prevent Vim from clearing a line with only blank characters (indentation).
  " TODO:
  "   Define more convenient macros for (un)commenting lines in OCaml.
  " TODO:
  "   write an intelligent “;” macro which is replaced by “in” when the line 
  "   starts with “let ”.
endfun

" Below, we provide a function, bound to <LocalLeader>m, which aligns the arrows 
" of a pattern matching construct. This is a pure Vim solution, using Merlin and 
" ocp-indent.
"
" In ~/.vim/UltiSnips/ocaml.snippets , we also provide a snippet which does this 
" as the user types. Unfortunately, for the snippet, all changes to the buffer 
" have to be done from a Python proxy, so that we need to duplicate most of the 
" code. In addition to the aforementioned plugins, the snippet depends on 
" UltiSnips and the Vim function OCamlSelectEnclosingPatternMatching() below.

" This function selects the closest pattern matching that contains the cursor. 
" It returns the line numbers [start, stop] of the selection, or [] if no 
" pattern matching construct was found.
" NOTE: This depends on the plugin Merlin.
fun! OCamlSelectEnclosingPatternMatching()
  " Enter Visual Mode from the current position of the cursor.
  normal! v
  let l:start = getpos("'<")
  let l:end   = getpos("'>")
  while v:true
    " Grow the selected expression, using Merlin.
    silent normal! :call merlin_visual#GrowInside('v')
    " Check that the selection has indeed grown, and fail otherwise.
    let l:start2 = getpos("'<")
    let l:end2   = getpos("'>")
    if l:start == l:start2 && l:end == l:end2
      normal! 
      return []
    endif
    let l:start = l:start2
    let l:end   = l:end2
    " Stop as soon as the word under the cursor is “match” or “function”.
    " NOTE: This uses the fact that the cursor is at the start of the selection.
    let l:word = expand('<cword>')
    if l:word ==# 'match' || l:word ==# 'function'
      break
    endif
    " For some reason, when the “match” construct is surrounded by “begin/end”, 
    " Merlin selects the last letter of “begin”, so we have to move the cursor 
    " ahead by one column.
    normal! l
    let l:word = expand('<cword>')
    if l:word ==# 'match' || l:word ==# 'function'
      break
    endif
    normal! h
  endwhile
  " Exit Visual Mode and returns the line numbers of the first and last lines.
  normal! 
  return [line("'<"), line("'>")]
endfun

" This function indents the pattern matching where the cursor lies, and aligns 
" its arrows.
fun! OCamlAlignPatternMatchingArrows()
  " Get the line numbers of the first and last lines of the pattern matching.
  let l:result = OCamlSelectEnclosingPatternMatching()
  if l:result == []
    return
  endif
  let [ l:start, l:end ] = l:result
  " Indent the lines.
  " NOTE: This depends on an indenting plugin for OCaml, such as “ocp-indent”.
  silent normal! gv=
  " Identify which lines will be aligned; roughly, they are those of the form 
  " “[indent] | [pattern] -> [expr]” where [indent] is the shortest possible 
  " among the lines considered. More accurately, the width of [indent] will be 
  " given by the first matching line, which is what we want if the user follows 
  " a reasonable coding style.
  let l:active_indent_width = -1 " the indent level of the marked lines
  let l:max_align_width = -1     " the number of columns at which to align arrows
  let l:marked_lines = []
  let l:lines = getline(l:start, l:end)
  for l:i in range(len(l:lines))
    " Identify candidate lines and chunk them.
    let l:match = matchlist(l:lines[l:i], '^\(\(\s*\)|[^|].\{-}\)\s*\(->.*\)$')
    if l:match == []
      continue
    endif
    let l:indent          = l:match[2]
    let l:before_arrow    = l:match[1]
    let l:arrow_and_after = l:match[3]
    let l:indent_width = strdisplaywidth(l:indent)
    let l:align_width  = strdisplaywidth(l:before_arrow)
    " If the indent level was not decided yet, then it is given by that line.
    if l:active_indent_width < 0
      let l:active_indent_width = l:indent_width
    endif
    " Only consider lines with the given indent level.
    if l:active_indent_width != l:indent_width
      continue
    endif
    " Mark the line for alignment (we need to know the maximal width before 
    " actually proceeding to alignment).
    if l:max_align_width < l:align_width
      let l:max_align_width = l:align_width
    endif
    call add(l:marked_lines, [l:i, l:before_arrow, l:arrow_and_after])
  endfor
  if l:active_indent_width < 0
    return
  endif
  " Align the arrows of the marked lines by inserting padding.
  let l:max_align_width += 1 " Leave at least one blank column before the arrows.
  let l:padding_fmt = '%-' . string(l:max_align_width) . 's'
  for [l:i, l:before_arrow, l:arrow_and_after] in l:marked_lines
    let l:padded_line = printf(l:padding_fmt, l:before_arrow) . l:arrow_and_after
    call setline(l:start + l:i, l:padded_line)
  endfor
endfun

" Dune (build system for OCaml): set shift width to 2 rather than the default 1:
au FileType dune setlocal expandtab tabstop=2 softtabstop=-1 shiftwidth=0

""""""""""""""""""""
""" Why3

execute 'set rtp^=' . g:opamshare . '/why3/vim'
" unfortunately the Vim material (only syntax highlighting) provided by the Why3 
" package (1) does not provide filetype detection, and (2) does not respect the 
" directory conventions of Vim… hence we do it ourselves…
" filetype detection:
au BufRead,BufNewFile *.mlw,*.why setlocal filetype=why3
" syntax highlighting:
au Syntax why3 runtime! why3.vim
" indentation rules:
au FileType why3 setlocal expandtab shiftwidth=2

""""""""""""""""""""
""" Coq

" hotkeys (<F2> Undo, <F3> Next, <F4> ToCursor):
au FileType coq call coquille#FNMapping()
" call :CoqLaunch to launch Coq.
" indentation rules:
au FileType coq setlocal expandtab shiftwidth=2

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Cool macros                                                                  "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Show the syntax-hilighting stack under the cursor.
" https://stackoverflow.com/questions/9464844/how-to-get-group-name-of-highlighting-under-cursor-in-vim
command! SynStack
  \ echo join(map(synstack(line('.'),col('.')), 'synIDattr(v:val,"name")'), ' ')

" Delete trailing whitespaces.
command! -range=% StripTrailing <line1>,<line2>call StripTrailingWhitespaces()
fun! StripTrailingWhitespaces() range
  if &ft =~? 'mail' || &ft =~? 'markdown'
    return
  endif
  let l:range = a:firstline . ',' . a:lastline
  execute l:range. 's/\m\s\+$//e'
endfun
" automatically delete trailing whitespaces when writing:
"au  BufWritePre * call StripTrailingWhitespaces()

" Decode percent-encoded strings such as %2C, %C3%A9, as found in URLs.
" From https://vi.stackexchange.com/questions/24547/decode-url-percent-decoding
" (An alternative is https://github.com/tpope/vim-unimpaired/ .)
command! -range DecodeURL <line1>,<line2>call DecodeURLPercent()
fun! DecodeURLPercent() range
  let l:save_gdefault = &gdefault
  let l:save_magic = &magic
  set nogdefault
  set magic
  let l:range = a:firstline . ',' . a:lastline
  execute 'command -buffer -nargs=* SubstInRange' l:range.'substitute <args>'
  SubstInRange/%\(\x\x\)/\=iconv(nr2char('0x' .. submatch(1)), 'utf-8', 'latin1')/ge
  delcommand SubstInRange
  let &gdefault = l:save_gdefault
  let &magic = l:save_magic
endfun

" A command to create a numbered list from the visually selected lines.
" Implementation notes:
"   - numbers are inserted at the 1st non-blank column of the current line (^); 
"     picking the 1st line is more tricky… let’s assume all lines are aligned.
"   - the key is g<C-a>, it has been added in Vim 8.
"     + the increment of g<C-a> is customizable, so we might add this feature to 
"       our commend.
"   - the mode testing is required to ensure that we are in Visual-Block Mode 
"     (we cannot issue <C-v> unconditionally, because if we already were in 
"     Visual-Block Mode, this would exit the mode).
xnoremap <expr>  <localleader>n
  \ (mode() ==# '<C-v>' ? '' : '<C-v>') .. '^I0. <Esc>gvg<C-a>'
" A different and more powerful implementation.
" Implementation notes:
"   - numbers are inserted at the 1st non-blank column of each line.
"   - we might adjust alignment of numbers depending on the number of digits?
"   - `:keepatterns` avoids polluting search; it has been added in Vim 7.4.
"   - we might add arguments to the command eg. to customize the initial number, 
"     the increment or the bullet format.
command! -nargs=0 -range  NumList
  \ keeppatterns :<line1>,<line2>smagic/^\s*\zs/\=(line('.') - <line1> + 1) .. '. '/

" A command to draw simple Unicode graphs, writing down ASCII characters and 
" substituting then afterwards. It applies only to the selected (Visual) area.
command! -range -bang Graph       silent! <line1>,<line2>call DrawUnicodeGraph('simple', "<bang>")
command! -range -bang GraphRound  silent! <line1>,<line2>call DrawUnicodeGraph('round', "<bang>")
command! -range -bang GraphBold   silent! <line1>,<line2>call DrawUnicodeGraph('bold', "<bang>")
command! -range       GraphDouble silent! <line1>,<line2>call DrawUnicodeGraph('double', '')
fun! DrawUnicodeGraph(style, dashed) range
  "
  " This input:
  "     /------.------\
  "     |      |      |
  "     |      |      |
  "     |      |      |
  "     }------+------{
  "     |      |      |
  "     |      |      |
  "     |      |      |
  "     `------^------'
  " produces the following output, according to the chosen style.
  " Indicated code points are in the range U+25xx.
  " http://www.fileformat.info/info/unicode/block/box_drawing/utf8test.htm
  "
  "   simple:                  simple dashed:
  "     ┌──────┬──────┐          ┌┄┄┄┄┄┄┬┄┄┄┄┄┄┐
  "     │0C  2C│    10│          ┊      ┊      ┊
  "     │      │      │          ┊      ┊      ┊
  "     │1C  3C│    24│          ┊      ┊      ┊
  "     ├──────┼──────┤          ├┄┄┄┄┄┄┼┄┄┄┄┄┄┤
  "     │      │  00  │          ┊      ┊  04  ┊
  "     │      │02    │          ┊      ┊0A    ┊
  "     │14  34│    18│          ┊      ┊      ┊
  "     └──────┴──────┘          └┄┄┄┄┄┄┴┄┄┄┄┄┄┘
  "   round:                   round dashed:
  "     ╭──────┬──────╮          ╭┄┄┄┄┄┄┬┄┄┄┄┄┄╮
  "     │6D    │    6E│          ┊      ┊      ┊
  "     │      │      │          ┊      ┊      ┊
  "     │      │      │          ┊      ┊      ┊
  "     ├──────┼──────┤          ├┄┄┄┄┄┄┼┄┄┄┄┄┄┤
  "     │      │      │          ┊      ┊      ┊
  "     │      │      │          ┊      ┊      ┊
  "     │70    │    6F│          ┊      ┊      ┊
  "     ╰──────┴──────╯          ╰┄┄┄┄┄┄┴┄┄┄┄┄┄╯
  "   bold:                    bold dashed:
  "     ┏━━━━━━┳━━━━━━┓          ┏┅┅┅┅┅┅┳┅┅┅┅┅┅┓
  "     ┃0F  33┃    13┃          ┋      ┋      ┋
  "     ┃      ┃      ┃          ┋      ┋      ┋
  "     ┃23  4B┃    2B┃          ┋      ┋      ┋
  "     ┣━━━━━━╋━━━━━━┫          ┣┅┅┅┅┅┅╋┅┅┅┅┅┅┫
  "     ┃      ┃  01  ┃          ┋      ┋  05  ┋
  "     ┃      ┃03    ┃          ┋      ┋03    ┋
  "     ┃17  3B┃    1B┃          ┋      ┋      ┋
  "     ┗━━━━━━┻━━━━━━┛          ┗┅┅┅┅┅┅┻┅┅┅┅┅┅┛
  "   double:
  "     ╔══════╦══════╗
  "     ║54  66║    57║
  "     ║      ║      ║
  "     ║60  6C║    63║
  "     ╠══════╬══════╣
  "     ║      ║  50  ║
  "     ║      ║51    ║
  "     ║5A  69║    5D║
  "     ╚══════╩══════╝
  "
  let l:save_gdefault = &gdefault
  let l:save_magic = &magic
  set nogdefault
  set magic
  let l:range = a:firstline . ',' . a:lastline
  execute 'command -buffer -nargs=* SubstInRange' l:range.'substitute <args>'
  if a:style ==# 'simple' || a:style ==# 'round'
    if a:dashed !=# ''
      SubstInRange/\%V-/┄/ge
      SubstInRange/\%V|/┊/ge
    else
      SubstInRange/\%V-/─/ge
      SubstInRange/\%V|/│/ge
    endif
    if a:style ==# 'round'
      SubstInRange/\%V\//╭/ge
      SubstInRange/\%V\\/╮/ge
      SubstInRange/\%V`/╰/ge
      SubstInRange/\%V'/╯/ge
    else
      SubstInRange/\%V\//┌/ge
      SubstInRange/\%V\\/┐/ge
      SubstInRange/\%V`/└/ge
      SubstInRange/\%V'/┘/ge
    endif
    SubstInRange/\%V\./┬/ge
    SubstInRange/\%V\^/┴/ge
    SubstInRange/\%V{/┤/ge
    SubstInRange/\%V}/├/ge
    SubstInRange/\%V+/┼/ge
  elseif a:style ==# 'bold'
    if a:dashed !=# ''
      SubstInRange/\%V-/┅/ge
      SubstInRange/\%V|/┋/ge
    else
      SubstInRange/\%V-/━/ge
      SubstInRange/\%V|/┃/ge
    endif
    SubstInRange/\%V\//┏/ge
    SubstInRange/\%V\\/┓/ge
    SubstInRange/\%V`/┗/ge
    SubstInRange/\%V'/┛/ge
    SubstInRange/\%V\./┳/ge
    SubstInRange/\%V\^/┻/ge
    SubstInRange/\%V{/┫/ge
    SubstInRange/\%V}/┣/ge
    SubstInRange/\%V+/╋/ge
  elseif a:style ==# 'double'
    SubstInRange/\%V-/═/ge
    SubstInRange/\%V|/║/ge
    SubstInRange/\%V\//╔/ge
    SubstInRange/\%V\\/╗/ge
    SubstInRange/\%V`/╚/ge
    SubstInRange/\%V'/╝/ge
    SubstInRange/\%V\./╦/ge
    SubstInRange/\%V\^/╩/ge
    SubstInRange/\%V{/╣/ge
    SubstInRange/\%V}/╠/ge
    SubstInRange/\%V+/╬/ge
  endif
  delcommand SubstInRange
  let &gdefault = l:save_gdefault
  let &magic = l:save_magic
endfun

" A command to fix French typography.
command! -range=% TypoFR <line1>,<line2>call TypoFR()
fun! TypoFR() range
  let l:save_gdefault = &gdefault
  let l:save_magic = &magic
  set nogdefault
  set magic
  let l:range = a:firstline . ',' . a:lastline
  execute 'command -buffer -nargs=* SubstInRange' l:range.'substitute <args>'
  " spaces around punctuation signs:
  SubstInRange/[[:space:]  ]\+\ze[,.]//ge
  SubstInRange/\.\.\+[[:space:]  ]*/… /ge
  SubstInRange/[[:space:]  ]*\ze[!?;]/ /ge
  SubstInRange/\I\zs[[:space:]  ]*:/ :/ige
  SubstInRange/? !/?!/ge
  SubstInRange/«\zs[[:space:]  ]*/ /ge
  SubstInRange/[[:space:]  ]*\ze»/ /ge
  " em‐dashes for dialogue and parentheses:
  SubstInRange/^-[[:space:]  ]*/— /e
  SubstInRange/ - / — /cge
  " quotes (guillemots and apostrophes):
  SubstInRange/"[[:space:]  ]*\(\_.\{-}\)[[:space:]  ]*"/« \1 »/ge
  SubstInRange/''[[:space:]  ]*\(\_.\{-}\)[[:space:]  ]*''/« \1 »/ge
  SubstInRange/'/’/ge
  " trailing spaces:
  execute l:range.'call StripTrailingWhitespaces()'
  " double spaces:
  SubstInRange/  / /ge
  " some numbers have their digits wrongly separated by spaces:
  SubstInRange/\d\zs\s\+\ze\d//cge
  " lowercase L instead of uppercase i is a common OCR problem:
  SubstInRange/\<ll/Il/Ige
  SubstInRange/l\ze\u/I/Ige
  " … as is the opposite:
  SubstInRange/\<I\ze\%(a\>\|es?\>\|’\)/l/Ige
  SubstInRange/\<II/Il/cIge
  SubstInRange/\l\zsI\+/\=substitute(submatch(0),'I','l','g')/Ige " replace (context)I^n by (context)l^n for any number n
  " spelling (ligatures):
  SubstInRange/oe/œ/cIge
  SubstInRange/O[Ee]/Œ/cIge
  " capitalize first letter of sentences:
  "SubstInRange/[.?!—][  \t\n]*\zs[a-zàâçéèêëîïñôùûÿæœ]/\u&/g
  SubstInRange/\%(\%^\|[.?!—>]\)[[:space:]  \n«»“”]*\zs[a-z[=a=][=c=][=e=][=i=][=n=][=o=][=u=][=y=]æœ]/\u&/Ige
  " for subtitle files: a more powerful regex which also detects when a sentence 
  " starts on a different frame than the previous sentence:
  SubstInRange/\%(\%^\|[.?!—>]\)\%([[:space:]  \n«»“”]\|^\d\+\n\d\d:\d\d:\d\d,\d\d\d --> \d\d:\d\d:\d\d,\d\d\d$\)*\zs[a-z[=a=][=c=][=e=][=i=][=n=][=o=][=u=][=y=]æœ]/\u&/Ige
  " spelling (accents on capital letters):
  SubstInRange/\<Ca\>/Ça/Ige
  SubstInRange/\<A\>/À/cIge
  SubstInRange/\<Etes\>/Êtes/Ige
  SubstInRange/\<E\ze[cpt][[=a=][=e=][=i=][=o=][=u=][=y=]æœ]/É/cIge
  delcommand SubstInRange
  let &gdefault = l:save_gdefault
  let &magic = l:save_magic
endfun

" TODO:
" ? upstream fix for `:keeppatterns :normal /` (see above)
" - upstream fix for motion ÷ (see above)
" - make motions /÷*× avoid polluting search (see above)
" - upstream fix 'langmap' (see above)
" - fix BÉPO mappings
" - deactivate smart parens in Replace Mode
" - command to rename current file
"   https://stackoverflow.com/questions/1205286/renaming-the-current-file-in-vim
" - make Ctrl-A, Ctrl-X use uppercase for hexadecimal by default
"   https://stackoverflow.com/questions/10341276/making-vim-use-capital-letters-when-incrementing-hexadecimal-numbers-with-ctrl-a
" - simplify our SubstInRange commands above to use :smagic or \m
"   (no need to save&reset the 'magic' option)
" - truncate tab titles with an ellipsis in the middle (does not seem possible?)
" - quick tab switcher by typing the buffer name (see: fzf.vim)
